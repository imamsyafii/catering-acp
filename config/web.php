<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'HealthStudio',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'jwt' => [
            'class' => \sizeg\jwt\Jwt::class,
            'key' => 'Pixilgift@secret1234!',
            // You have to configure ValidationData informing all claims you want to validate the token.
            'jwtValidationData' => \app\components\JwtValidationData::class,
        ],
        
/*
	    'view' => [
	         'theme' => [
	             'pathMap' => [
	                '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
	             ],
	         ],
	    ],
*/
	    'assetManager' => [
	        'bundles' => [
	            'dmstr\web\AdminLteAsset' => [
	                'skin' => 'skin-yellow-light',
	            ],
	        ],
	    ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '-kg-zdCdL5F-k-06cZgzRAX7RRBwa7vf',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
        'class' => 'webvimark\modules\UserManagement\components\UserConfig',
       
        'on beforeRequest' => function () {
        \Yii::$app->catchAll = [
            'site/maintenance',
        ];
        },
        
        // Comment this if you don't want to record user logins
        'on afterLogin' => function($event) {
		        \webvimark\modules\UserManagement\models\UserVisitLog::saveDetailtoSession();
                \webvimark\modules\UserManagement\models\UserVisitLog::newVisitor($event->identity->id);
            }
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
        	//'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //'urlFormat'=>'path'
            
            'rules'=>[
            	
                'GET api/<model:\w+>' => 'api/list',
                'GET api/<model:\w+>/<id:\d+>' => 'api/view',
                'POST api/<model:\w+>'=>'api/create',
                
                'GET api/u/<model:\w+>' => 'api/personallist',
                'GET api/u/<model:\w+>/<id:\d+>' => 'api/personalview',
                'POST api/u/<model:\w+>'=>'api/personalcreate',

                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
					  
                   ],
                   
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'modules'=>[
      'user-management' => [
        'class' => 'webvimark\modules\UserManagement\UserManagementModule',
       
        
        'passwordConfig' =>array(
        
        'minimumPassword' => 6,
        'maximumPassword' => 20,
        'lowerCaseContain'=> true,
        'upperCaseContain'=> true,
        'numberContain'=> true,
        'symbolContain'=> false,
        'usernameCharFilter' => 5,
        'passwordExpiryDay' => 300,
        
        'passwordHistory' => 4, 
        'bannedOnPasswordExpired'=>false,
        'failedAttemptsLimit'=>10,
        'maxAttempts'=>15,
        'maxAttemptFailedExpiry'=>60,
            ),
            'userCanHaveMultipleRoles' =>false,
        //'notUsedExpiryDay' => 0,
        'auditTrail'=>false,
        // Here you can set your handler to change layout for any controller or action
        // Tip: you can use this event in any module
        'on beforeAction'=>function(yii\base\ActionEvent $event) {
//                print_r($event->action->uniqueId);exit;
             //return $this->redirect('/some/url',302)->send();
            // webvimark\modules\UserManagement\models\UserVisitLog::LoginStatusValidation();
                if ( $event->action->uniqueId == 'user-management/auth/login' )
                {
                    $event->action->controller->layout = 'loginLayout.php';
                }
            },
		],
    ],
                
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
