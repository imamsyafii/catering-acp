<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    
    
    'knet'=>[
//        'resource_key'=>'9175649433069175',
//        'transport_id'=>'125801',
//        'transport_password'=>'125801pg',
        'resource_key'=>'UKTTW38581P1SR62',
        'transport_id'=>'120701',
        'transport_password'=>'120701pg',
        'action_code'=>'1',
        'language'=>'USA',
        'currency'=>'414',
        'response_url'=>'https://apps.pixilapps.com/test/response.php',
        'error_url'=>'https://apps.pixilapps.com/test/response.php',

        'development_url'=>'https://kpaytest.com.kw/kpg/PaymentHTTP.htm',
        'production_url'=>'https://www.kpay.com.kw/kpg/PaymentHTTP.htm',
        
        'is_production'=>false,
    ],
    
];
