<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_friendship".
 *
 * @property string $id
 * @property int $main_id
 * @property int $secondary_id
 * @property int $type
 * @property int $status
 * @property int $is_blocked
 * @property string $created_at
 * @property string $updated_at
 */
class UserFriendship extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_friendship';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['main_id', 'secondary_id', 'type', 'status', 'is_blocked'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'main_id' => 'Main ID',
            'secondary_id' => 'Secondary ID',
            'type' => 'Type',
            'status' => 'Status',
            'is_blocked' => 'Is Blocked',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public static function getAllFriends($vars, $lang="en")
    {
	    $return = array("error"=>1, "message"=>"No friend found.", "details"=>[]);
	    
	    $type = array("request", "confirmed");
	    $friend_data = [];
	    $friends = UserFriendship::find()
	    ->select("user_friendship.*, C2.first_name as C2firstname, C2.last_name as C2lastname, C.first_name as Cfirstname, C.last_name as Clastname")
	    ->leftJoin("customer C", "C.id=user_friendship.main_id")
	    ->leftJoin("customer C2", "C2.id=user_friendship.secondary_id")
	    ->where("main_id=:id or secondary_id=:id",['id'=>$vars['user_id']])->asArray()->all();
	    
	    for($a=0; $a<count($friends); $a++)
	    {
		    //$friend[$a] = $friends[$a];
		    if($friends[$a]['main_id']==$vars['user_id'])
		    {
			   $friend[$a] = array(
					"user_id"=>$friends[$a]['main_id'],
					"friend_firstname"=>$friends[$a]['C2firstname'],
					"friend_lastname"=>$friends[$a]['C2lastname'],
					"type" => $friends[$a]['type'],
			   );
		    }
		    else
		    {
			    $friend[$a] = array(
					"user_id"=>$friends[$a]['secondary_id'],
					"friend_firstname"=>$friends[$a]['Cfirstname'],
					"friend_lastname"=>$friends[$a]['Clastname'],
					"type" => $friends[$a]['type'],
			   );
		    }
		    
		    $friendtype = $friend[$a]['type'];
		    $friend_data[$type[$friendtype]][] = $friend[$a];
	    }
	    
	    if(!empty($friend_data))
	    {
		    $return = array("error"=>0, "message"=>"Friend found.", "details"=>$friend_data);
	    }
	    
	    return $return;
    }
}
