<?php

namespace app\models;

use Yii;
use app\components\KnetPayment\KnetPayment;
use yii\helpers\Url;
/**
 * This is the model class for table "package".
 *
 * @property int $id
 * @property string|null $title_en
 * @property string|null $title_ar
 * @property string|null $description_en
 * @property string|null $description_ar
 * @property string|null $image
 * @property int|null $status
 */
class Package extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $image_src;
    public static function tableName()
    {
        return 'package';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description_en', 'description_ar'], 'string'],
            [['status'], 'integer'],
            [['title_en', 'title_ar', 'status'], 'required'],
            [['title_en', 'title_ar', 'image','description_en', 'description_ar'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_en' => 'Title (English)',
            'title_ar' => 'Title (Arabic)',
            'description_en' => 'Description (English)',
            'description_ar' => 'Description (Arabic)',
            'image' => 'Image',
            'image_src' => 'Image',
            'status' => 'Status',
        ];
    }
    
    public static function getAllPackages($vars){
        $extrafilter = "";
        if(isset($vars['category_id'])){
            if(!empty($vars['category_id']))
            $extrafilter = " and PCD.category_id in($vars[category_id])";
        }
        $packages = Package::find()
                ->where("package.status=1 $extrafilter")
                ->leftJoin("package_category_detail PCD","PCD.package_id=package.id")
                ->asArray()
                ->all();
        if(empty($packages))
        {
            $code = 1;
            $mess = "Package not found";
        }
        else{
            $code = 0;
            $mess = "Package found";
        }
        
        $return = new APIResponse($code, $mess, $packages);
        return $return->format();
    }
    
    public static function getPackageDetail($vars){
        $extrafilter = "";
        if(isset($vars['package_id'])){
            if(!empty($vars['package_id']))
            $extrafilter = " and package.id in($vars[package_id])";
        }
        $packages = Package::find()
                ->where("package.status=1 $extrafilter")
                ->leftJoin("package_category_detail PCD","PCD.package_id=package.id")
                ->asArray()
                ->all();
        if(empty($packages))
        {
            $code = 1;
            $mess = "Package not found";
        }
        else{
            for($a=0; $a<count($packages); $a++){
                $packages[$a]['menu'] = Menu::getAllMenu(array('package_id'=>$packages[$a]['id']));
            }
            $code = 0;
            $mess = "Package found";
        }
        
        $return = new APIResponse($code, $mess, $packages);
        return $return->format();
    }
    
    
        public static function doOrder($vars)
    {
	    //$order = Order::find()->where("id='$vars[id]'",[])->asArray()->one();
        //return Url::to(['knet/knetresponse'],'http');
        $ref = date("YmdHis").rand(1000, 9999);
        $knet = new KnetPayment();
        $knet->response_url = 'https://apps.pixilapps.com/knet-response.php';//Url::to(['knet/knetresponse'],'http'); // back response    
        
        $knet->error_url = Url::to(['public/knetresult'],'http'); // back error
        $knet->amount = 10; // your ammount or cost you can use decimal 10.33
        //$knet->ref = $ref;
        $knet->udf1 = "UDF 1";
        $knet->udf2 = "UDF 2";
        $knet->udf3 = "UDF 3";
        $knet->udf4 = "UDF 4";
        $knet->udf5 = "UDF 5";
        $knet->action = 1;//leav it
        $knet->currency = 414;//Kw dinar
        $knet->lang = "ENG"; //default arabic lang
        $knet->alias = "masiya";
        //print_r($knet);exit;
        $req_param = json_encode($knet);
        $result = $knet->run(); // return json data
        $result = json_decode($result);
        $req_result = json_encode($result);

	        if(isset($result->url))
	        {
//	        	$upd_order = \app\models\Order::find()
//	                    ->where("order_reference=:ref", ['ref'=>$ref])
//	                    ->one();
//	            
//	            if($upd_order)
//			    {
//			    $upd_order->payment_request_json	= json_encode(array('param'=> $req_param, 'result' => $req_result));
//				$upd_order->payment_method	= "KNET";
//				$upd_order->payment_id 		= $result->payment_id;
//				$upd_order->payment_url 	= $result->url;
//				$upd_order->save();
//				
//				
//				/*
//				Yii::$app->mail->compose()
//		                ->setTo('didats@gmail.com')
//		                ->setFrom(['noreply@masiya.net' => 'OOR'])
//		                ->setSubject('Masiya Finance Orders Payment Request')
//		                ->setHtmlBody($upd_order->PaymentJson)
//		                
//		                ->send();*/
//		                
//				}
				
		        $returnUrl = $result->url;
	        }
	        else
	        {
		        $returnUrl = $baseURL."/public/paymentfailed";
	        }
        
		return array('paymentURL'=>$returnUrl,
			'responseUrl'=>$knet->response_url,
                        'result'=>$req_result,
                    'referenceID'=>'');
	}
    
    
}
