<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;
use Yii;
/**
 * Description of uploadImage
 *
 * @author imam
 */

class uploadImage {
    private $image;
    private $dir;
    private $image_name;
    //put your code here
    function __construct($image, $dir) {
        $this->image = $image;
        $this->dir = $dir;
        
    }
    
    function execute() {
        $image = $this->image;
        $directory = $this->dir;
        
        $image_parse = (explode(".", $image->name));
        $ext = end($image_parse);
         // generate a unique file name to prevent duplicate filenames
         $image_name = Yii::$app->security->generateRandomString().".{$ext}";
         Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/upload_file/'.$directory.'/';
         if (is_dir( Yii::$app->params['uploadPath'])){
         }
         else{
             mkdir( Yii::$app->params['uploadPath'], 0755, true);
         }

         $path = Yii::$app->params['uploadPath'] . $image_name;
         if($image->saveAs($path)){
             return $image_name;
         }
         else {
             return NULL;
         }
    }
}