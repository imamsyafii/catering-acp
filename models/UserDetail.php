<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_detail".
 *
 * @property int $id
 * @property string $fullname
 * @property string $email
 * @property string $phone_number
 * @property int $address_id
 * @property int $shop_id
 * @property double $latitude
 * @property double $longitude
 */
class UserDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address_id', 'shop_id'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['fullname'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 100],
            [['phone_number'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Fullname',
            'email' => 'Email',
            'phone_number' => 'Phone Number',
            'address_id' => 'Address ID',
            'shop_id' => 'Shop ID',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ];
    }
}
