<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $order_reference
 * @property int|null $category_id
 * @property int|null $package_id
 * @property string|null $order_from
 * @property string|null $order_to
 * @property int|null $address_id
 * @property string|null $order_status
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'category_id', 'package_id', 'address_id'], 'integer'],
            [['order_from', 'order_to', 'created_at', 'updated_at'], 'safe'],
            [['order_reference', 'order_status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Customer',
            'order_reference' => 'Order Reference',
            'category_id' => 'Category',
            'package_id' => 'Package',
            'order_from' => 'Order From',
            'order_to' => 'Order To',
            'address_id' => 'Address ID',
            'order_status' => 'Order Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
