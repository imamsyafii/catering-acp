<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu_availability".
 *
 * @property int $id
 * @property string|null $date_from
 * @property string|null $date_to
 * @property int|null $package_id
 * @property int|null $menu_id
 */
class MenuAvailability extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu_availability';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_from', 'date_to'], 'safe'],
            [['package_id', 'menu_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'package_id' => 'Package',
            'menu_id' => 'Menu',
        ];
    }
}
