<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "commons".
 *
 * @property int $id
 * @property string $common_key
 * @property string $common_value
 * @property string $description
 */
class Commons extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'commons';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['common_value'], 'required'],
            [['common_value', 'description'], 'string'],
            [['common_key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'common_key' => 'Common Key',
            'common_value' => 'Common Value',
            'description' => 'Description',
        ];
    }
    
    public static function getAllCommons()
    {
	    $return = array("error"=>1, "message"=>"No common setting found.", "details"=>[]);
	    
	    $commons = Commons::find()->asArray()->all();
	    if(!empty($commons))
	    {
		    $return = array("error"=>0, "message"=>"Common setting found.", "details"=>$commons);
		    return $commons;
	    }
	    
	    return $return;
    }
}
