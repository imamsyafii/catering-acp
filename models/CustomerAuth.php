<?php

namespace app\models;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $work_phone
 * @property string $home_phone
 * @property string $mobile_phone
 * @property string $gender
 * @property string $date_of_birth
 * @property string $reg_type
 * @property string $social_id
 * @property string $avatar
 * @property string $username
 * @property string $password
 * @property int $status
 * @property int $username_base
 * @property int $sms
 * @property int $newsletter
 * @property string $created_at
 * @property string $updated_at
 * @property string $email_temp
 * @property int $reputation
 */
class CustomerAuth extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['gender'], 'string'],
            [['date_of_birth', 'created_at', 'updated_at'], 'safe'],
            [['status', 'username_base', 'sms', 'newsletter', 'reputation'], 'integer'],
            [['first_name', 'last_name', 'email'], 'string', 'max' => 255],
            [['work_phone', 'home_phone', 'mobile_phone', 'social_id'], 'string', 'max' => 50],
            [['reg_type'], 'string', 'max' => 25],
            [['avatar', 'username', 'password'], 'string', 'max' => 150],
            [['email_temp'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'work_phone' => 'Work Phone',
            'home_phone' => 'Home Phone',
            'mobile_phone' => 'Mobile Phone',
            'gender' => 'Gender',
            'date_of_birth' => 'Date Of Birth',
            'reg_type' => 'Reg Type',
            'social_id' => 'Social ID',
            'avatar' => 'Avatar',
            'username' => 'Username',
            'password' => 'Password',
            'status' => 'Status',
            'username_base' => 'Username Base',
            'sms' => 'Sms',
            'newsletter' => 'Newsletter',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'email_temp' => 'Email Temp',
            'reputation' => 'Reputation',
        ];
    }
    
    public static function findById($id)
    {
	    $user = CustomerAuth::find()->select("id, email as username, auth_key as authKey, password, auth_key as accessToken")->where(['id'=>$id])->asArray()->one();
	    if(!empty($user))
	   {
		   $user['role'] = "Customer";
	   }
	   return $user;
    }
    
    public static function findByToken($token)
    {
	    $user = CustomerAuth::find()->select("id, email as username, auth_key as authKey, password, auth_key as accessToken")->where(['auth_key'=>$token])->asArray()->one();
	    if(!empty($user))
	   {
		   $user['role'] = "Customer";
	   }
	   return $user;
    }
    
    public static function findByUsername($username)
    {
	    $user = CustomerAuth::find()->select("id, email as username, auth_key as authKey, password, auth_key as accessToken")->where(['email'=>$username])->asArray()->one();
	    if(!empty($user))
	   {
		   $user['role'] = "Customer";
	   }
	   return $user;
    }
}