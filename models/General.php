<?php
namespace app\models;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of General
 *
 * @author pixil
 */
class General {

    public static function checkForNull($row_sql, $fromFetchAll=1)
{
//print_r($row_sql);exit;
	if($fromFetchAll==1)
	{
		for($a = 0; $a<count($row_sql); $a++)
	        {
	        	        	
	        	foreach ($row_sql[$a] as $key => $value) {
                                if (!is_object($row_sql[$a]))
                                {
				    if(!is_array($value))
                                    {
				    if (($value == NULL))
				    {
				    //	$value = trim($value);
				        $row_sql[$a][$key] = "";
				        }
                                        else if(!is_array ($value))
                                        {
                                                $row_sql[$a][$key] = $value."";
                                        }
                                    }
                                }
                                else
                                {
                                    if (empty($row_sql[$a]->$key))
                                    {
                                        $row_sql[$a]->$key = $row_sql[$a]->$key.'';
                                    }
                                }
                            }  
			}
	}
	else
	{
		if(!empty($row_sql))
                {
		foreach ($row_sql as $key => $value) {    
                    if (($value == NULL))
                    {
                    //	$value = trim($value);
                        $row_sql[$key] = "";
                        }
                    }
                }
	}
	return $row_sql;
    }


   public static function custom_sort($a,$b) {
          return $a['category_id']>$b['category_id'];
     }
    //bitly url
    public static function returnShortUrl($value) {
       /*
 $value_arr = json_decode(Yii::app()->bitly->shorten($value)->getResponseData());
        if ($value_arr->data)
            $value_url = $value_arr->data->url; ///////url:...
        else
            $value_url = "o";
        return $value_url;
*/

 $value_arr = json_decode(Yii::app()->bitly->shorten($value)->getResponseData());
        if ($value_arr->data)
        {
            $value_url = $value_arr->data->url; ///////url:...
        }
        else
        {
            $value_url = "o";
        }
       
        $url = parse_url($value);
		if(isset($url['scheme'])&&$url['scheme'] == 'https'){
			$count = 1;
             $value_url = str_replace("http://", "https://", $value_url, $count);
		}
            
        return $value_url;
        
    }

    public static function returnAdminUser() {
        return array('tripper');
    }

//type 1 as photo and 0 as video
    public static function uploadFileToServer($key, $type, $lang = 0, $for_post = 0) {
        // Where the file is going to be placed 
        if ($for_post == 0){
              //$target_path = Yii::app()->basePath . "/../upload/avatar/";
              //$target_path_thum = Yii::app()->basePath."/../upload/avatar/thums/";
              $target_path = getcwd()."/upload/avatar/";
              $target_path_thum = getcwd()."/upload/avatar/thums/";
            }
        else{
              //$target_path = Yii::app()->basePath . "/../upload/post/";
              //$target_path_thum = Yii::app()->basePath."/../upload/post/thums/";
              $target_path = getcwd()."/upload/post/";
              $target_path_thum = getcwd()."/upload/post/thums/";
            }

        if (isset($_FILES[$key])) {

            for ($i = 0; $i < count($_FILES[$key]['name']); $i++) {

                $tmpFilePath = $_FILES[$key]['tmp_name'];

                //Make sure we have a filepath
                if ($tmpFilePath != "") {
                    //Setup our new file path
                    //$newFilePath = $target_path .basename( $_FILES['upload']['name']);
                    //Upload the file into the temp dir
                    $new_name = General::generateRandom();
                    $agent = (isset($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : "Android" ;
                    //$agent="ios";
                    $error = "";

                    $ext = explode('.', $_FILES[$key]["name"]);

                    if (strtolower($agent) == "android") {
                        if ($type == 1)//photo
                        {
                            $error = General::checkExtensionsImagesAndroid(mb_strtolower(end($ext)), $lang);
                        }
                        else{
                            $error = General::checkExtensionsVideoAndroid(mb_strtolower(end($ext)), $lang);
                        }
                    }
                    else {
                        if (strtolower($agent) == "ios"){
                            if ($type == 1){
                                $error = General::checkExtensionsImagesIos(mb_strtolower(end($ext)), $lang);
                            }
                            else{
                                $error = General::checkExtensionsVideoIos(mb_strtolower(end($ext)), $lang);
                            }
                        }
                    }
                    if ($error != "")
                    {
                        throw new Exception($error);
                    }

                    $file_name = $target_path . $new_name . '.' . mb_strtolower(end($ext));
                    $thum_file_name = $target_path_thum.$new_name.'.'.mb_strtolower(end($ext));

                    if (move_uploaded_file($tmpFilePath, $file_name)) {
                        $new_file = $new_name . '.' . mb_strtolower(end($ext));

                        //$this->ResizeToDimension(300, $file_name, $new_file);
//                  General::ResizeToDimension(300, $file_name, $new_file);
                       if($for_post==0){
                       General::ResizeToDimension(200, $file_name, $thum_file_name);}
                       else{
                       General::ResizeToDimension(640, $file_name, $thum_file_name);}
                          
                        return $new_name . '.' . mb_strtolower(end($ext));
                    }
                }
            }
        } else {

            throw new Exception(GeneralVar::$no_uploaded_file[$lang] . $key); //here there is exception and adding will not be done
        }
    }

    public static function checkExtensionsVideoIos($ext, $lang = 0) {
        if (strtolower($ext) == "m4v" || strtolower($ext) == "mov" || strtolower($ext) == "mp4"){
            return "";
        }
        else{
            return GeneralVar::$video_ios[$lang];
        }
    }

    public static function checkExtensionsVideoAndroid($ext, $lang = 0) {
        if (strtolower($ext) == "3gp" || strtolower($ext) == "mp4"){
            return "";
        }
        else{
            return GeneralVar::$video_android[$lang];
        }
    }

    public static function checkExtensionsImagesIos($ext, $lang = 0) {
        if (strtolower($ext) == "png" || strtolower($ext) == "jpeg" || strtolower($ext) == "jpg"){
            return "";
        }
        else{
            return GeneralVar::$image_ios[$lang];
        }
    }

    public static function checkExtensionsImagesAndroid($ext, $lang = 0) {
        if (strtolower($ext) == "png" || strtolower($ext) == "jpeg" || strtolower($ext) == "jpg")
        {
            return "";
        }
        else{
            return GeneralVar::$image_android[$lang];
        }
    }

    public static function isImage($ext) {
        if (strtolower($ext) == "png" || strtolower($ext) == "jpeg" || strtolower($ext) == "jpg"){
            return true;
        }
        else{
            return false;
        }
    }

    public static function generateRandomCode() {
        $length = 5;
        $randomString = substr(str_shuffle("0123456789"), 0, $length);
        return $randomString;
    }

    public static function generateRandom() {
        $length = 10;
        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
        return $randomString;
    }

    public static function generateRandomNumbers() {
        $length = 10;
        $randomString = substr(str_shuffle("0123456789"), 0, $length);
        return $randomString;
    }

    public static function encryption($str) {
        if (function_exists('hash'))
        {
            $pass = hash('sha256', $str);
        }
        return $pass;
    }

    public static function encrypt_decrypt($action, $string) {
        $output = false;

        $key = 'My_strong_random_secret_key';

        // initialization vector 
        $iv = md5(md5($key));

        if ($action == 'encrypt') {
            $output = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($string), MCRYPT_MODE_CBC, $iv);
            $output = rtrim($output, "");
        }
        return $output;
    }

    public static function checkUniqueVal($val, $model_name, $col_name) {
        $val = strtolower($val);
        $criteria = new CDbCriteria;
        $criteria->select = '' . $col_name . '';
        $criteria->condition = "LOWER(" . $col_name . ")='" . $val . "'";
        $res = $model_name::model()->find($criteria);

        if ($res) {
            if ($res->attributes['' . $col_name . ''] != '') {

                return 0;
            } else {

                return 1;
            }
        } else
        {
            return 1;        
        }
    }

    public static function checkUniqueValElse($val, $model_name, $col_name, $id) {
        $val = strtolower($val);
        $criteria = new CDbCriteria;
        $criteria->select = '' . $col_name . '';
        $criteria->condition = "LOWER(" . $col_name . ")='" . $val . "' and id!=" . $id;
        $res = $model_name::model()->find($criteria);

        if ($res) {
            if ($res->attributes['' . $col_name . ''] != '') {

                return 0;
            } else {

                return 1;
            }
        } else{
            return 1;
        }
    }

    public static function checkValidMail($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return 1;
        } else{
            return 0;
        }
    }

    public static function checkExistsMail($email) {
        $res = verify::jValidateEmailUsingSMTP($email, GeneralVar::$generalhost, GeneralVar::$publicmail, true); // ($email, GeneralVar::$publicmail);

        if ($res == ""){
            return 0;
        }
        else{
            return 1;
        }
    }

    public static function differentBetweenTwoDates($date1, $date2) {
        // $date1 = "2008-11-01 22:45:00"; 
        // $date2 = "2009-12-04 13:44:01"; 
        $diff = abs(strtotime($date2) - strtotime($date1));

        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

        $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));

        $minuts = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);

        $seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minuts * 60));


        $total_days = floor($diff / (60 * 60 * 24));

        $array_date = array();
        $array_date['years'] = $years;
        $array_date['months'] = $months;
        $array_date['days'] = $days;
        $array_date['totaldays'] = $total_days;
        $array_date['hours'] = $hours;
        $array_date['minutes'] = $minuts;
        $array_date['seconds'] = $seconds;

        return $array_date;
    }

    public static function numberOfHoursBetweenTwoDates($date1, $date2) {
        $t1 = StrToTime($date1);
        $t2 = StrToTime($date2);

        $diff = $t1 - $t2;
        $hours = $diff / ( 60 * 60 );
        return $hours;
    }

    public static function checkLangParameter($lang) {
        if (strcmp($lang, "1") == 0)
        {
            return 1;
        }
        if (strcmp($lang, "0") == 0)
        {
            return 1;
        }

        return 0;
    }

    public static function is_number($num) {
        return is_numeric($num);
    }

    public static function checkPassedParameters($pass_param, $lang) {
        foreach ($pass_param as $key => $val) {
            $field = $val['field'];
            $what_check = $val['check'];
            ///1---------
            if ($what_check == "number") {
                if (!General::is_number($field)) {
                    return $field . " " . GeneralVar::$error_number[$lang];
                }
            }
            //2-----------
            if ($what_check == "lang") {
                if (!General::checkLangParameter($field)){
                    return GeneralVar::$error_lang[0];
                }
            }
            //3-----------
            if ($what_check == "favourite") {
                if (!General::checkFavouriteParameter($field)){
                    return GeneralVar::$error_favourite[$lang];
                }
            }
            //4---------------
            if ($what_check == "service_phone_chat") {
                if (!General::checkPhoneChatParameter($field)){
                    return GeneralVar::$error_phone_chat_serive[$lang];
                }
            }
            //5---------------
            if ($what_check == "is_active") {
                if (!General::checkIsActiveDevice($field)){
                    return GeneralVar::$error_is_active[$lang];
                }
            }
            //6---------------
            if ($what_check == "device_type") {
                if (!General::checkDeviceType($field)){
                    return GeneralVar::$error_device_type[$lang];
                }
            }
            //7-----------
            if ($what_check == "lang_device") {
                if (!General::checkLangParameter($field))
                {
                    return GeneralVar::$error_lang_device[0];
                }
            }
        }
        return "1";
    }

    /////1,2,2,4 check if string in this syntax 
    public static function isCorrectListOfNumbers() {
        
    }

    ///// asd:{0,1}:{0,1}
    public static function isCorrectList() {
        
    }

    public static function checkFavouriteParameter($is_favourite) {
        if (strcmp($is_favourite, "1") == 0)
        {
            return 1;
        }
        if (strcmp($is_favourite, "0") == 0)
        {
            return 1;
        }

        return 0;
    }

    public static function checkPhoneChatParameter($service) {
        if (strcmp($service, "1") == 0)
        {
            return 1;
        }
        if (strcmp($service, "0") == 0)
        {
            return 1;
        }

        return 0;
    }

    public static function checkIsActiveDevice($active) {
        if (strcmp($active, "1") == 0)
        {
            return 1;
        }
        if (strcmp($active, "0") == 0)
        {
            return 1;
        }

        return 0;
    }

    public static function checkDeviceType($type) {
        if (strcmp($type, "ios") == 0)
        {
            return 1;
        }
        if (strcmp($type, "android") == 0)
        {
            return 1;
        }

        return 0;
    }

    public static function parseUrlPost($url, $data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        print_r($result);
    }

    public static function parseUlrFaceBook($name) {
        $url = "graph.facebook.com/" . $name . "/picture?type=large&redirect=false";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $avatarInfo = curl_exec($ch);
        curl_close($ch);
        $DefaultAvatar = '';
        if ($avatarInfo) {

            $avatarInfo = json_decode($avatarInfo);

            if (isset($avatarInfo->data->url)){
                $DefaultAvatar = $avatarInfo->data->url;
            }
        }
        return $DefaultAvatar;
    }

    function getTwitterAvatar($twitterUserName) {
        $options = array(
            CURLOPT_HEADER => TRUE, // Final URL returned in the header
            CURLOPT_FOLLOWLOCATION => TRUE, // We need the ultimate destination
            CURLOPT_NOBODY => TRUE, // We don't care about the content
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_CONNECTTIMEOUT => 10, // Keep small to not delay page rendering
            CURLOPT_TIMEOUT => 10, // Keep small to not delay page rendering
            CURLOPT_MAXREDIRS => 5 // Shouldn't be more than 1 redirect, but just in case
        );
        $ch = curl_init('http://api.twitter.com/1/users/profile_image/' . $twitterUserName . '.json');
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);
        $imgSrc = $header['url'];

        if (strrpos($imgSrc, 'http') !== FALSE) {
            // Return Twitter avatar
            return $imgSrc;
        } else {
            // Return generic image
            return 'https://si3.twimg.com/sticky/default_profile_images/default_profile_4_reasonably_small.png';
        }
    }

    public static function searchInArray($key_we_search, $array_ids) {
        foreach ($array_ids as $k => $v) {
            if ($v["key"] == $key_we_search)
            {
                return $v["val"];
            }
        }
    }

    public static function ResizeToDimension($dimension, $source, $destination) {
        //get the image size
        $size = getimagesize($source);

        //determine dimensions
        $width = $size[0];
        $height = $size[1];

        //determine what the file extension of the source
        //image is
        $extension = "jpg";
        switch ($size[2]) {
            case IMAGETYPE_GIF:
                $sourceImage = imagecreatefromgif($source);
                $extension = "gif";
                break;
            case IMAGETYPE_JPEG:
                $sourceImage = imagecreatefromjpeg($source);
                $extension = "jpg";
                break;
            case IMAGETYPE_PNG:
                $sourceImage = imagecreatefrompng($source);
                $extension = "png";
                break;
            default: return false;
        }

        // find the largest dimension of the image
        // then calculate the resize perc based upon that dimension
        $percentage = ( $width >= $height ) ? 100 / $width * $dimension : 100 / $height * $dimension;

        // define new width / height
        $newWidth = $width / 100 * $percentage;
        $newHeight = $height / 100 * $percentage;

        // create a new image
        $destinationImage = imagecreatetruecolor($newWidth, $newHeight);

        // copy resampled
        imagecopyresampled($destinationImage, $sourceImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        //exif only supports jpg in our supported file types
        if ($extension == "jpg" || $extension == "jpeg") {

            //fix photos taken on cameras that have incorrect
            //dimensions
            $exif = exif_read_data($source);

            //get the orientation
            $ort = isset($exif['Orientation']) ? $exif['Orientation'] : 1;

            //determine what oreientation the image was taken at
            switch ($ort) {

                case 2: // horizontal flip

                    $this->ImageFlip($dimg);

                    break;

                case 3: // 180 rotate left

                    $destinationImage = imagerotate($destinationImage, 180, -1);

                    break;

                case 4: // vertical flip

                    $this->ImageFlip($dimg);

                    break;

                case 5: // vertical flip + 90 rotate right

                    $this->ImageFlip($destinationImage);

                    $destinationImage = imagerotate($destinationImage, -90, -1);

                    break;

                case 6: // 90 rotate right

                    $destinationImage = imagerotate($destinationImage, -90, -1);

                    break;

                case 7: // horizontal flip + 90 rotate right

                    $this->ImageFlip($destinationImage);

                    $destinationImage = imagerotate($destinationImage, -90, -1);

                    break;

                case 8: // 90 rotate left

                    $destinationImage = imagerotate($destinationImage, 90, -1);

                    break;
            }
        }

        // create the jpeg
        return imagejpeg($destinationImage, $destination, 100);
    }

    public function ImageFlip(&$image, $x = 0, $y = 0, $width = null, $height = null) {

        if ($width < 1)
        {
            $width = imagesx($image);
        }
        if ($height < 1)
        {
            $height = imagesy($image);
        }

        // Truecolor provides better results, if possible.
        if (function_exists('imageistruecolor') && imageistruecolor($image)) {

            $tmp = imagecreatetruecolor(1, $height);
        } else {

            $tmp = imagecreate(1, $height);
        }

        $x2 = $x + $width - 1;

        for ($i = (int) floor(($width - 1) / 2); $i >= 0; $i--) {

            // Backup right stripe.
            imagecopy($tmp, $image, 0, 0, $x2 - $i, $y, 1, $height);

            // Copy left stripe to the right.
            imagecopy($image, $image, $x2 - $i, $y, $x + $i, $y, 1, $height);

            // Copy backuped right stripe to the left.
            imagecopy($image, $tmp, $x + $i, $y, 0, 0, 1, $height);
        }

        imagedestroy($tmp);

        return true;
    }

    public static function smart_resize_image($file, $width = 0, $height = 0, $proportional = false, $output = 'file', $delete_original = true, $use_linux_commands = false) {

        if ($height <= 0 && $width <= 0)
            return false;

        # Setting defaults and meta
        $info = getimagesize($file);

        $image = '';
        $final_width = 0;
        $final_height = 0;

        switch ($info[2]) {
            case IMAGETYPE_GIF:
                $image = imagecreatefromgif($file);
                break;
            case IMAGETYPE_JPEG:
                $image = imagecreatefromjpeg($file);
                break;
            case IMAGETYPE_PNG:
                $image = imagecreatefrompng($file);
                break;
            default: return false;
        }

        list($width_old, $height_old) = $info;
        // the source image should be rectangle
        $proportional = 1;

        // rotate the image
        $image = imagerotate($image, 270, 0);

        # Calculating proportionality

        if ($proportional) {
            if ($width == 0)
            {
                $factor = $height / $height_old;
            }
            elseif ($height == 0){
                $factor = $width / $width_old;
            }
            else{
                $factor = min($width / $width_old, $height / $height_old);
            }

            $final_width = round($width_old * $factor);
            $final_height = round($height_old * $factor);
        }
        else {
            $final_width = ( $width <= 0 ) ? $width_old : $width;
            $final_height = ( $height <= 0 ) ? $height_old : $height;
        }

        // check the width and height
        //$ratio  = $height / $height_old;
        # This is the resizing/resampling/transparency-preserving magic
        $image_resized = imagecreatetruecolor($final_width, $final_height);
        if (($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG)) {
            $transparency = imagecolortransparent($image);

            if ($transparency >= 0) {
                $transparent_color = imagecolorsforindex($image, $trnprt_indx);
                $transparency = imagecolorallocate($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
                imagefill($image_resized, 0, 0, $transparency);
                imagecolortransparent($image_resized, $transparency);
            } elseif ($info[2] == IMAGETYPE_PNG) {
                imagealphablending($image_resized, false);
                $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
                imagefill($image_resized, 0, 0, $color);
                imagesavealpha($image_resized, true);
            }
        }



        imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);

        # Taking care of original, if needed
        if ($delete_original) {
            if ($use_linux_commands)
            {
                exec('rm ' . $file);
            }
            else
            {
                @unlink($file);
            }
        }

        # Preparing a method of providing result
        switch (strtolower($output)) {
            case 'browser':
                $mime = image_type_to_mime_type($info[2]);
                header("Content-type: $mime");
                $output = NULL;
                break;
            case 'file':
                $output = $file;
                break;
            case 'return':
                return $image_resized;
                break;
            default:
                break;
        }

        # Writing image according to type to the output destination

        switch ($info[2]) {
            case IMAGETYPE_GIF: imagegif($image_resized, $output);
                break;
            case IMAGETYPE_JPEG: imagejpeg($image_resized, $output);
                break;
            case IMAGETYPE_PNG: imagepng($image_resized, $output);
                break;
            default: return false;
        }

        return true;
    }

    public static function imageRenderView($img) {
        if ($img)
        {
            return CHtml::image($img, "", array("width" => 200, 'height' => 150, "style" => "height:150px;width:200px"));
        }
    }
    
    //imam 
    public static function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
    
    public static function time_elapsed_string_NEW_FAIL($ptime)
{
    // Past time as MySQL DATETIME value
    $ptime = strtotime($ptime);

    // Current time as MySQL DATETIME value
    $csqltime = date('Y-m-d H:i:s');

    // Current time as Unix timestamp
    $ctime = strtotime($csqltime); 

    // Elapsed time
    $etime = $ctime - $ptime;

    // If no elapsed time, return 0
    if ($etime < 1){
        return '0 seconds';
    }

    $a = array( 365 * 24 * 60 * 60  =>  'year',
                 30 * 24 * 60 * 60  =>  'month',
                      24 * 60 * 60  =>  'day',
                           60 * 60  =>  'hour',
                                60  =>  'minute',
                                 1  =>  'second'
    );

    $a_plural = array( 'year'   => 'years',
                       'month'  => 'months',
                       'day'    => 'days',
                       'hour'   => 'hours',
                       'minute' => 'minutes',
                       'second' => 'seconds'
    );

    foreach ($a as $secs => $str){
        // Divide elapsed time by seconds
        $d = $etime / $secs;
        if ($d >= 1){
            // Round to the next lowest integer 
            $r = floor($d);
            // Calculate time to remove from elapsed time
            $rtime = $r * $secs;
            // Recalculate and store elapsed time for next loop
            if(($etime - $rtime)  < 0){
                $etime -= ($r - 1) * $secs;
            }
            else{
                $etime -= $rtime;
            }
            // Create string to return
            $estring = $estring . $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ';
        }
    }
    return $estring . ' ago';
}

}
