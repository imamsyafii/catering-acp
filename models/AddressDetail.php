<?php

namespace app\models;
use Yii;
/**
 * This is the model class for table "address_detail".
 *
 * @property int $id
 * @property string $area_en
 * @property string $area_ar
 * @property string $block
 * @property string $street
 * @property string $avenue
 * @property string $building
 * @property string $floor
 * @property int $address_type
 * @property string $apartement_number
 * @property string $additional_direction
 */
class AddressDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'address_type'], 'integer'],
            [['area_en', 'area_ar', 'block', 'avenue', 'building', 'floor', 'apartement_number'], 'string', 'max' => 20],
            [['street'], 'string', 'max' => 40],
            [['additional_direction'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area' => 'Area',
            'block' => 'Block',
            'street' => 'Street',
            'avenue' => 'Avenue',
            'building' => 'Building',
            'floor' => 'Floor',
            'address_type' => 'Address Type',
            'apartement_number' => 'Apartement Number',
            'additional_direction' => 'Additional Direction',
        ];
    }
}
