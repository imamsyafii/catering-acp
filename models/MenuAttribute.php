<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu_attribute".
 *
 * @property int $id
 * @property string|null $title_en
 * @property string|null $title_ar
 * @property string|null $description_en
 * @property string|null $description_ar
 * @property string|null $image
 * @property int|null $status
 */
class MenuAttribute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu_attribute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description_en', 'description_ar'], 'string'],
            [['status'], 'integer'],
            [['title_en', 'title_ar', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_en' => 'Title En',
            'title_ar' => 'Title Ar',
            'description_en' => 'Description En',
            'description_ar' => 'Description Ar',
            'image' => 'Image',
            'status' => 'Status',
        ];
    }
    
    public static function getAllAttributes($vars){
        $extrafilter = "";
        if(isset($vars['menu_id'])){
            if(!empty($vars['menu_id']))
            $extrafilter = " and MAD.menu_id in($vars[menu_id])";
        }
        $attributes = MenuAttribute::find()
                ->select("menu_attribute.id, menu_attribute.title_en, menu_attribute.title_ar, MAD.value")
                ->where("menu_attribute.status=1 $extrafilter")
                ->leftJoin("menu_attribute_value MAD","MAD.attribute_id=menu_attribute.id")
                ->asArray()
                ->all();
        if(empty($attributes))
        {
            $code = 1;
            $mess = "Attribute not found";
        }
        else{
            $code = 0;
            $mess = "Attribute found";
        }
        
        return $attributes;
        //$return = new APIResponse($code, $mess, $packages);
        //return $return->format();
    }
}
