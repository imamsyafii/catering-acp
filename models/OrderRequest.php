<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_request".
 *
 * @property int $id
 * @property string|null $order_reference
 * @property float|null $order_amount
 * @property string|null $payment_method
 * @property string|null $payment_status
 * @property int|null $category_id
 * @property int|null $package_id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $user_id
 * @property int|null $address_id
 */
class OrderRequest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_amount'], 'number'],
            [['category_id', 'package_id', 'user_id', 'address_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['order_reference'], 'string', 'max' => 255],
            [['payment_method', 'payment_status'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_reference' => 'Order Reference',
            'order_amount' => 'Order Amount',
            'payment_method' => 'Payment Method',
            'payment_status' => 'Payment Status',
            'category_id' => 'Category ID',
            'package_id' => 'Package ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
            'address_id' => 'Address ID',
        ];
    }
}
