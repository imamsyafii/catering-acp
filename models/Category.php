<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string|null $title_en
 * @property string|null $title_ar
 * @property string|null $description_en
 * @property string|null $description_ar
 * @property string|null $image
 * @property int|null $status
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $image_src;
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description_en', 'description_ar'], 'string'],
            [['status'], 'integer'],
            [['title_en', 'title_ar', 'status'], 'required'],
            [['title_en', 'title_ar', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_en' => 'Title (English)',
            'title_ar' => 'Title (Arabic)',
            'description_en' => 'Description (English)',
            'description_ar' => 'Description (Arabic)',
            'image' => 'Image',
            'status' => 'Status',
        ];
    }
    public static function getAllCategories($vars)
    {
        $categories = Category::find()
                ->where("Status=1")
                ->asArray()
                ->all();
        if(empty($categories))
        {
            $code = 1;
            $mess = "Categories not found";
        }
        else{
            $code = 0;
            $mess = "Categories found";
        }
        
        $return = new APIResponse($code, $mess, $categories);
        return $return->format();
    }
}
