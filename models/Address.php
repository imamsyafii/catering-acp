<?php

namespace app\models;
use Yii;
/**
 * This is the model class for table "address".
 *
 * @property int $id
 * @property int $city_id
 * @property string $description_en
 * @property string $description_ar
 * @property double $latitude
 * @property double $longitude
 * @property int $status
 * @property int $is_default
 * @property string $user_agent
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id', 'status', 'is_default'], 'integer'],
            [['latitude', 'longitude'], 'required'],
            [['latitude', 'longitude'], 'number'],
            [['description_en', 'description_ar'], 'string', 'max' => 255],
            [['user_agent'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'description_en' => 'English Description',
            'description_ar' => 'Arabic Description',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'status' => 'Status',
        ];
    }
    
    
    public static function updateAddress($vars, $lang='en')
    {
        $return = array('error'=>0, 'messages'=>'Success');
        
        $city = \app\models\City::find()->where(['id'=>$vars['city_id']])->one();
        $user_address = \app\models\UserAddress::find()->where(['user_id'=>$vars['user_id']])->asArray()->all();
        $user_address_ids = array_column($user_address, 'address_id');
        
        if(isset($vars['is_default'])){
            if($vars['is_default']=='1'){
               Address::updateAll(['is_default' => 0], ['and', ['is_default' => '1'  ],['id' => $user_address_ids  ]  ]);
 
            }
        }
        
        if($vars['is_new']=='0')
        {
            //update address here
            $add = Address::find()->where(['id'=>$vars['address_id']])->one();
            $add->description_en = $vars['address_name'];
            $add->description_ar = $vars['address_name'];
            $add->latitude  = isset($vars['latitude'])?$vars['latitude']:0;
            $add->longitude  = isset($vars['longitude'])?$vars['longitude']:0;
            $add->city_id = $vars['city_id'];
            $add->user_agent = json_encode($_SERVER['HTTP_USER_AGENT']);
            $add->is_default  = isset($vars['is_default'])?$vars['is_default']:0;
            $add->save();
            
            
            
            $add_det = \app\models\AddressDetail::find()->where(['id'=>$vars['address_id']])->one();
            if(empty($add_det))
            {
            	$add_det = new \app\models\AddressDetail();
            }
            $add_det->id = $add->id;
            $add_det->area_en = $city->name_en;//isset($vars['area']);
            $add_det->area_ar = $city->name_ar;
            $add_det->block = $vars['block'];
            $add_det->street = $vars['street'];
            $add_det->avenue = isset($vars['avenue'])?$vars['avenue']:NULL;
            $add_det->building = $vars['building'];
            $add_det->floor = $vars['floor'];
            $add_det->address_type = $vars['address_type'];
            $add_det->apartement_number = isset($vars['apartement_number'])?$vars['apartement_number']:NULL;
            $add_det->additional_direction = isset($vars['additional_direction'])?$vars['additional_direction']:NULL;
            $add_det->save();
            $message = "Address updated successfully";
        }
        else
        {
            //create new address here
            $headers = Yii::$app->request->headers;
            $agent = $headers['HTTP_USER_AGENT'];
            $add = new Address();
            $add->description_en = $vars['address_name'];
            $add->description_ar = $vars['address_name'];
            $add->latitude  = isset($vars['latitude'])?$vars['latitude']:0;
            $add->longitude  = isset($vars['longitude'])?$vars['longitude']:0;
            $add->city_id = $vars['city_id'];
            $add->user_agent = json_encode($agent);
            $add->is_default  = isset($vars['is_default'])?$vars['is_default']:0;
            if($add->save())
            {}
            else
            {
                $return['errors'] = ($add->getErrors());
            }
            
            $add_det = new \app\models\AddressDetail();
            $add_det->id = $add->id;
			$add_det->area_en = $city->name_en;//isset($vars['area']);
            $add_det->area_ar = $city->name_ar;
            $add_det->block = $vars['block'];
            $add_det->street = $vars['street'];
            $add_det->avenue = isset($vars['avenue'])?$vars['avenue']:NULL;
            $add_det->building = $vars['building'];
            $add_det->floor = $vars['floor'];
            $add_det->address_type = $vars['address_type'];
            $add_det->apartement_number = isset($vars['apartement_number'])?$vars['apartement_number']:NULL;
            $add_det->additional_direction = isset($vars['additional_direction'])?$vars['additional_direction']:NULL;            if($add_det->save())
            {}
            else
            {
            	$return['errors_on_detail'] = $add_det->getErrors();
            }
            
            $return['vars'] = $vars;
            $customer_address = new \app\models\UserAddress();
            $customer_address->address_id = $add->id;
            $customer_address->user_id = $vars['user_id'];
            $customer_address->status = '1';
            $customer_address->save();
            $message = "Address added successfully";
        }
	$address = Address::CustAddress(['user_id'=>$vars['user_id']], $lang);
        $response = new APIResponse(0, isset($message)?$message:"", $address['details']);
        return $response->format();
    }
    
    
    public static function UserAddress($vars,$lang="en")
    {
        $response = new APIResponse(1, "No address found.", []);
        $address = Address::find()->asArray()->all();
        if(!empty($address))
        {
                $address = General::checkForNull($address);
                $response = new APIResponse(0, "Address found.", $address);
        }

        return $response->format();

    }
    
    public static function CustAddress($vars,$lang="en")
    {
	$response = new APIResponse(1, "No address found.", []);
	    
	$addids = array();
        $customer_address = \app\models\UserAddress::find()->where(['user_id'=>$vars['user_id'], 'status'=>'1'])->orderBy(['address_id'=>SORT_DESC])->asArray()->all();
        for($a=0; $a<count($customer_address); $a++)
        {
            $addids[] = $customer_address[$a]['address_id'];
        }
        $address = Address::find()->where(['id'=>$addids])->orderBy(['id'=>SORT_DESC])->asArray()->all();
        
        for($a=0; $a<count($address); $a++)
        {
            $address[$a]['is_default'] = !empty($address[$a]['is_default'])?$address[$a]['is_default']:'0'; 
            $add_detail = \app\models\AddressDetail::find()->where(['id'=>$address[$a]['id']])->one();
            if(!empty($add_detail))
            {
                foreach($add_detail as $key => $val)
                {
                    $address[$a][$key] = $val; 
                }
            }
        }
        
        if(!empty($address))
        {
            //$return = array("error"=>0, "message"=>"Address found.", "details"=>$return['details']);
            $address = General::checkForNull($address,1);
            $response = new APIResponse(0, "Address found.", $address);
        }
	
        //$return['details'][0]['data'] = General::checkForNull($return['details'][0]['data'],1);
    
	return $response->format();

    }
    
    public static function deleteAddress($vars, $lang='en')
    {
        
        
        $message = "Delete isn't successful";
        $customer_address = \app\models\UserAddress::find()->where(['user_id'=>$vars['user_id'], 'address_id'=>$vars['address_id']])->one();
        if(empty($customer_address))
        {
            $message = "No address found";
            $response = new APIResponse(1, $message, []);
            return $response->format();
        }
        $customer_address->status = '0';
        if($customer_address->save())
        {
            $message = "Delete Successfully";
        }
        
        $addids = array();
	$return = Address::CustAddress(['user_id'=>$vars['user_id']], $lang);
        //$return['details'][0]['data'] = General::checkForNull($return['details'][0]['data'],1);
        $response = new APIResponse(0, $message, $return['details']);
        return $response->format();
    }
    public static function setDefaultAddress($vars, $lang='en')
    {
        $user_address = \app\models\UserAddress::find()->where(['user_id'=>$vars['user_id']])->asArray()->all();
        $user_address_ids = array_column($user_address, 'address_id');
        
        
        
        $update_address = Address::updateAll(['is_default' => 0], ['and', ['is_default' => '1'  ],['id' => $user_address_ids  ]  ]);
 
        $message = "Set as default unsuccessful";
        $customer_address = \app\models\Address::find()->where(['id'=>$vars['address_id']])->one();
        $customer_address->is_default = '1';
        if($customer_address->save())
        {
            $message = "Set as default successfully";
        }
        
        $addids = array();
	$return = Address::CustAddress(['user_id'=>$vars['user_id']], $lang);
        //$return['details'][0]['data'] = General::checkForNull($return['details'][0]['data'],1);
        $response = new APIResponse(0, $message, $return['details']);
        return $response->format();
    }
    
}
