<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "devices".
 *
 * @property int $device_id
 * @property string $app_identifier
 * @property string $app_version
 * @property string $device_type
 * @property string $device_token
 * @property string $is_active
 * @property int $lang
 * @property string $timezone
 * @property string $created_date
 * @property string $updated_date
 * @property string $user_agent
 * @property int $user_id
 */
class Devices extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'devices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['app_identifier', 'app_version', 'device_type', 'device_token'], 'required'],
            [['is_active', 'user_agent'], 'string'],
            [[ 'user_id'], 'integer'],
            [['lang','created_date', 'updated_date'], 'safe'],
            [['app_identifier', 'device_token', 'timezone'], 'string', 'max' => 255],
            [['app_version', 'device_type'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'device_id' => 'Device ID',
            'app_identifier' => 'App Identifier',
            'app_version' => 'App Version',
            'device_type' => 'Device Type',
            'device_token' => 'Device Token',
            'is_active' => 'Is Active',
            'lang' => 'Lang',
            'timezone' => 'Timezone',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
            'user_agent' => 'User Agent',
            'user_id' => 'User ID',
        ];
    }
    
    public static function addDevice($vars, $lang)
    {
	    $device = new Devices();
	    
	    $device->app_identifier = $vars['app_identifier'];
	    $device->app_version = $vars['app_version'];
	    $device->device_type = $vars['device_type'];
	    $device->device_token = $vars['device_token'];
	    $device->is_active = '1';
	    $device->lang = $lang;
	    $device->created_date = date("Y-m-d H:i:s");
	    $device->updated_date = date("Y-m-d H:i:s");
	    $device->user_agent = json_encode($_SERVER['HTTP_USER_AGENT']);
	    
	    if($device->save())
	    {
		    if(isset($vars['user_id']))
		    {
				$device->user_id = $vars['user_id'];
			    $device->save();
			    
			    //Devices::updateAll(['user_id' => $vars['user_id']], ['=', 'device_token', $device->device_token]);
		    }
		    return Devices::find()->where("device_id = :deviceid",['deviceid'=>$device->device_id])->asArray()->all();
	    }
	    else
	    	return $device->getErrors();
	    
	    
	    //return [1,22,3,4,5];
    }
}
