<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "package_attribute_value".
 *
 * @property int $id
 * @property int|null $attribute_id
 * @property string|null $value
 * @property int|null $status
 */
class PackageAttributeValue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'package_attribute_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_id', 'status'], 'integer'],
            [['value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attribute_id' => 'Attribute ID',
            'value' => 'Value',
            'status' => 'Status',
        ];
    }
}
