<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property string|null $title_en
 * @property string|null $title_ar
 * @property string|null $description_en
 * @property string|null $description_ar
 * @property string|null $image
 * @property int|null $package_id
 * @property int|null $status
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $image_src;
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description_en', 'description_ar'], 'string'],
            [['package_id', 'status'], 'integer'],
            [['title_en', 'title_ar'], 'required'],
            [['title_en', 'title_ar', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_en' => 'Title (English)',
            'title_ar' => 'Title (Arabic)',
            'description_en' => 'Description (English)',
            'description_ar' => 'Description (Arabic)',
            'image' => 'Image',
            'image_src' => 'Image',
            'package_id' => 'Package ID',
            'status' => 'Status',
        ];
    }
    public static function getAllMenu($vars){
        $extrafilter = "";
        if(isset($vars['package_id'])){
            if(!empty($vars['package_id']))
            $extrafilter = " and menu.package_id in($vars[package_id])";
        }
        $menu = Menu::find()
                ->where("menu.status=1 $extrafilter")
               // ->leftJoin("package_category_detail PCD","PCD.package_id=package.id")
                ->asArray()
                ->all();
        if(empty($menu))
        {
            $code = 1;
            $mess = "Menu not found";
        }
        else{
            $code = 0;
            $mess = "Menu found";
            for($a=0; $a<count($menu); $a++)
            {
                $menu[$a]['attributes'] = MenuAttribute::getAllAttributes(['menu_id'=>$menu[$a]['id']]);
            }
        }
        return $menu;
        //$return = new APIResponse($code, $mess, $packages);
        //return $return->format();
    }
}
