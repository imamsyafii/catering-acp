<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use Yii;

class JWTAuthenticator {

    public static function getToken($id, $type="Customer") {
        $jwt = Yii::$app->jwt;
        $signer = $jwt->getSigner('HS256');
        $key = $jwt->getKey();
        $time = time();
        $token = $jwt->getBuilder()
                ->issuedBy('http://giftogo.com')
                ->permittedFor('http://giftogo.com')
                ->issuedAt($time)
//                ->expiresAt($time + 1)// Configures the expiration time of the token (exp claim)
                ->withClaim('uid', $id)
                ->withClaim('type',$type)
                ->getToken($signer, $key);
        return $token;
    }
    
}
