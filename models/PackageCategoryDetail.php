<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "package_category_detail".
 *
 * @property int $id
 * @property int|null $package_id
 * @property int|null $category_id
 * @property float|null $price
 * @property int|null $status
 */
class PackageCategoryDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'package_category_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['package_id', 'category_id', 'status'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'package_id' => 'Package ID',
            'category_id' => 'Category ID',
            'price' => 'Price',
            'status' => 'Status',
        ];
    }
}
