<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;
    
class APIResponse {

    private $code;
    private $message;
    private $details;

    function __construct($code, $message, $details) {
        $this->code = $code;
        $this->message = $message;
        $this->details = $details;
    }

    function format() {
        return ["code" => $this->code, "details" => $this->details, "message" => $this->message];
    }

}
