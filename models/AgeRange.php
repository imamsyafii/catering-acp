<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "age_range".
 *
 * @property int $id
 * @property string|null $title_en
 * @property string|null $title_ar
 * @property string|null $description_en
 * @property string|null $description_ar
 * @property int|null $status
 */
class AgeRange extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'age_range';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description_en', 'description_ar'], 'string'],
            [['status'], 'integer'],
            [['title_en', 'title_ar', 'status'], 'required'],
            [['title_en', 'title_ar'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_en' => 'English Title',
            'title_ar' => 'Arabic Title',
            'description_en' => 'English Description',
            'description_ar' => 'Arabic Description',
            'status' => 'Status',
        ];
    }
}
