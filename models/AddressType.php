<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "address_type".
 *
 * @property int $AddressTypeId
 * @property string $EnglishName
 * @property string $ArabicName
 * @property string $Status
 * @property string $Icon
 */
class AddressType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['EnglishName', 'ArabicName'], 'required'],
            [['Status'], 'string'],
            [['EnglishName', 'ArabicName'], 'string', 'max' => 30],
            [['Icon'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AddressTypeId' => 'Address Type ID',
            'EnglishName' => 'English Name',
            'ArabicName' => 'Arabic Name',
            'Status' => 'Status',
            'Icon' => 'Icon',
        ];
    }
    
    public static function getAllTypes()
    {
	    $data = AddressType::find()->asArray()->all();
	    $return = General::checkForNull($data);
	    return $return;
    }
}
