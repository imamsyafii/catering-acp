<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property int $state_id
 * @property int $country_id
 * @property string $name_en
 * @property string $name_ar
 * @property string $ascii
 * @property double $latitude
 * @property double $longitude
 * @property string $timezoneid
 * @property int $status
 * @property string $included_cities
 * @property int $related_city_id
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state_id', 'country_id', 'status', 'related_city_id'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['included_cities'], 'string'],
            [['name_en', 'name_ar', 'ascii', 'timezoneid'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state_id' => 'State ID',
            'country_id' => 'Country ID',
            'name_en' => 'Name En',
            'name_ar' => 'Name Ar',
            'ascii' => 'Ascii',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'timezoneid' => 'Timezoneid',
            'status' => 'status',
            'included_cities' => 'Included Cities',
            'related_city_id' => 'Related City ID',
        ];
    }
    
    public static function getAllCities($vars = array())
    {
	    $filter = "";
	    if(isset($vars['country_id']))
	    {
		    $filter .= " and country_id='$vars[country_id]' ";
	    }
	    
	    $data = City::find()->where("id!='' $filter")->asArray()->all();
	    $data = General::checkForNull($data);
	    return $data;
    }
    
public static function getNearestCityV2($vars=array())
        {
            //$request = Yii::$app->request;
            //$post = $request->post();
            
            $headers = Yii::$app->request->headers;
           

            $latitute =  $headers['latitude'];// $_SERVER['HTTP_LATITUDE'];
            $longitude = $headers['longitude'];//$_SERVER['HTTP_LONGITUDE'];
            
	    $getLocationArea = \app\models\Location::getAreaByLocation($latitute, $longitude);
            //return  $getLocationArea;
            $getLocationAreaAr = \app\models\Location::getAreaByLocation($latitute, $longitude,'ar');
            

            if(empty($getLocationArea))
            {
                    return array();
                    //return array('error'=>1,'message'=>'Not Found', 'details'=>[]);
            }
            $post['newcity'] = ($getLocationArea['city']);
            $post['newstate'] = ($getLocationArea['state']);
            $post['newcountry'] = ($getLocationArea['country']);
            
            
            $post['newcityar'] = ($getLocationAreaAr['city']);
            $post['newstatear'] = ($getLocationAreaAr['state']);
            $post['newcountryar'] = ($getLocationAreaAr['country']);
            
            $country_id = isset($post['country_id'])?$post['country_id']:0;
            $state_id = isset($post['state_id'])?$post['state_id']:0;
            $city_id = isset($post['CityId'])?$post['CityId']:0;
            
            
            $included_city_id = NULL;
            
            $statefilterbylocation = "and state.name_en='$post[newstate]'";
            if($post['newstate']=="Kuwait City")
            {
            	$statefilterbylocation = "and (state.name_en='$post[newstate]' or state.name_en='Kuwait')";
            }
            
            $check_city =  Yii::$app->db->createCommand("select * from city
left join state on state.id=city.state_id
left join country on country.id=city.country_id
where city.name_en = '$post[newcity]' $statefilterbylocation and country.name_en='$post[newcountry]' and city.status='1'; ")->queryAll(); 

			//check city the next level Oct 15 2018
			if(empty($check_city))
			{
				$getLocationArea = \app\models\Location::getAreaByLocation($latitute, $longitude,NULL, 1);
	            //return  $getLocationArea;
	            $getLocationAreaAr = \app\models\Location::getAreaByLocation($latitute, $longitude,'ar', 1);
	            
	            
	            $post['newcity'] = ($getLocationArea['city']);
	            $post['newstate'] = ($getLocationArea['state']);
	            $post['newcountry'] = ($getLocationArea['country']);
	            
	            
	            $post['newcityar'] = ($getLocationAreaAr['city']);
	            $post['newstatear'] = ($getLocationAreaAr['state']);
	            $post['newcountryar'] = ($getLocationAreaAr['country']);
	            
                    $country_id = isset($post['country_id'])?$post['country_id']:0;
	            $state_id = isset($post['state_id'])?$post['state_id']:0;
	            $city_id = isset($post['city_id'])?$post['city_id']:0;
	            
	            
	            $included_city_id = NULL;
	            
	            $statefilterbylocation = "and state.name_en='$post[newstate]'";
	            if($post['newstate']=="Kuwait City")
	            	$statefilterbylocation = "and (state.name_en='$post[newstate]' or state.name_en='Kuwait')";
	            
	            $check_city =  Yii::$app->db->createCommand("select * from city
                    left join state on state.id=city.state_id
                    left join country on country.id=city.country_id
                    where city.name_en = '$post[newcity]' $statefilterbylocation and country.name_en='$post[newcountry]'; ")->queryAll(); 
			$included_city_id = isset($check_city[0]['CityId'])?$check_city[0]['CityId']:NULL;

			}
			
			

			if(empty($check_city))
			{
				$check_city2 =  Yii::$app->db->createCommand("select * from city
				left join state on state.id=city.state_id
left join country on country.id=city.country_id
				where city.included_cities like '%$post[newcity]%' and country.name_en='$post[newcountry]'; ")->queryAll(); 
				
				if(!empty($check_city2))
				{
					$included_city_id = $check_city2[0]['CityId'];
				}
				else
				return array();
			}
            //print_r($_POST);exit;
			
            
            if(!empty($post['newcountry']))
	        	{
	        		$country =  \app\models\Country::find()->where('name_en=:name',['name'=>$post['newcountry']])->one();
	        		if(!$country)
	        		{
		        	$country = new \app\models\Country();
		        	$country->name_en = $post['newcountry'];
		        	$country->name_ar = $post['newcountryar'];
		        	$country->dial_code = '000';
		        	$country->country_shipping = 0;
		        	$country->status = 0;
		        	if($country->save())
		        	{}else
		        	{
			        	return ($country->getErrors());exit;
		        	}
		        	}
		        	$country_id = $country->id;
	        	}
				$newstate = ($post['newstate']);
				//return $country_id;
	        	if(!empty($post['newstate']))
	        	{
	        		
	        		$state =  \app\models\State::find()->where("name_en='$newstate'",[])->one();
	        		
	        		//print_r($country_id);exit;
					if(empty($state))
	        		{
	        		
		        	$state = new \app\models\State();
		        	
		        	$state->name_en = $post['newstate'];
		        	$state->name_ar = $post['newstatear'];
		        	
		        	$state->country_id = $country_id;
		        	//print_r($newstate);exit;
		        	$state->status = 1;
		        	
		        	if($state->save())
		        	{
			        	
		        	}else
		        	{
			        	echo "State error:";print_r($state->getErrors());exit;
		        	}
		        	}
					//var_dump($state);
		        	$state_id = $state->id;
	        	}
				//print_r($state_id);exit;
				if(!empty($included_city_id))
				{
					$city_id = $included_city_id;
				}
				else
				{
		        	if(!empty($post['newcity']))
		        	{
		        		$city =  \app\models\City::find()->where('name_en=:name and status=:status',['name'=>$post['newcity'], 'status'=>'1'])->one();
		        		if(!$city)
		        		{
			        	$city = new \app\models\City();
			        	$city->name_en = $post['newcity'];
			        	$city->name_ar = $post['newcityar'];
			        	$city->country_id = $country_id;
			        	$city->state_id = $state_id;
			        	$city->status = 1;
			        	$city->Latitude = $latitute;
						$city->Longitude = $longitude;
			        	if($city->save())
			        	{}else
			        	{
				        	print_r($city->getErrors());exit;
			        	}
			        	}
			        	$city_id = $city->id;
			        	
		        	} 
	        	}
	        	
	        	
	        $extraFilter = "";

        	if(isset($vars['country_id']))
        	{
	        	$extraFilter .= " and city.country_id='$vars[country_id]' ";
        	}
        	
        	if(isset($city_id))
        	{
	        	$extraFilter .= " and city.id='$city_id' ";
        	}
        	$latitute = $_SERVER['HTTP_LATITUDE'];
                $longitude =$_SERVER['HTTP_LONGITUDE'];

            
            
            $lat_column = 'city.Latitude';
            $lon_column = 'city.Longitude';
        	
        	$distance = "( 6371 * acos( cos( radians($latitute) ) * cos( radians( $lat_column) ) * cos( radians( $lon_column ) - radians($longitude) ) + sin( radians($latitute) ) * sin( radians( $lat_column ) ) ) ) AS distance";
        	
        	
        	
        	$city = City::find()
                ->select("city.*, $distance")
                ->where("id!='' and city.status='1' $extraFilter ",[])
                ->having("distance!='' or distance=0",[])
                ->orderBy(['distance'=> SORT_ASC])

                ->asArray()->one();
                if(!empty($city))
                {
                    $return =  General::checkForNull(array($city),0); 
                }
		else
                {
		        $return = array();
                }
		        
		        //$return['test'] = $getLocationArea;
	        return $return;
	        
        }
}
