<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $confirmation_token
 * @property int $status
 * @property int $superadmin
 * @property int $created_at
 * @property int $updated_at
 * @property int $lastpass_changed
 * @property string $last_wrong_login
 * @property int $wrong_count
 * @property string $registration_ip
 * @property string $bind_to_ip
 * @property string $email
 * @property int $email_confirmed
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthItem[] $itemNames
 * @property UserVisitLog[] $userVisitLogs
 */
class UserAuth extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'created_at', 'updated_at'], 'required'],
            [['status', 'superadmin', 'created_at', 'updated_at', 'lastpass_changed', 'wrong_count', 'email_confirmed'], 'integer'],
            [['last_wrong_login'], 'safe'],
            [['username', 'password_hash', 'confirmation_token', 'bind_to_ip'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['registration_ip'], 'string', 'max' => 15],
            [['email'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'confirmation_token' => 'Confirmation Token',
            'status' => 'Status',
            'superadmin' => 'Superadmin',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'lastpass_changed' => 'Lastpass Changed',
            'last_wrong_login' => 'Last Wrong Login',
            'wrong_count' => 'Wrong Count',
            'registration_ip' => 'Registration Ip',
            'bind_to_ip' => 'Bind To Ip',
            'email' => 'Email',
            'email_confirmed' => 'Email Confirmed',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemNames()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])->viaTable('auth_assignment', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVisitLogs()
    {
        return $this->hasMany(UserVisitLog::className(), ['user_id' => 'id']);
    }
    
    public static function findById($id)
    {
	   $user = UserAuth::find()->select("id, username, auth_key as authKey, password_hash as password, auth_key as accessToken")->where(['id'=>$id])->asArray()->one();
	   if(!empty($user))
	   {
		   $user['role'] = "Admin";
	   }
	   return $user;
    }
    
    public static function findByToken($token)
    {
	   $user = UserAuth::find()->select("id, username, auth_key as authKey, password_hash as password, auth_key as accessToken")->where(['auth_key'=>$token])->asArray()->one();
	   if(!empty($user))
	   {
		   $user['role'] = "Admin";
	   }
	   return $user;
    }
    
    public static function findByUsername($username)
    {
	   $user = UserAuth::find()->select("id, username, auth_key as authKey, password_hash as password, auth_key as accessToken")->where(['username'=>$username])->asArray()->one();
	   if(!empty($user))
	   {
		   $user['role'] = "Admin";
	   }
	   return $user;
    }
}
