<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu_attribute_value".
 *
 * @property int $id
 * @property int|null $attribute_id
 * @property int|null $menu_id
 * @property string|null $value
 * @property int|null $status
 */
class MenuAttributeValue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu_attribute_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_id', 'menu_id', 'status'], 'integer'],
            [['value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attribute_id' => 'Attribute ID',
            'menu_id' => 'Menu ID',
            'value' => 'Value',
            'status' => 'Status',
        ];
    }
}
