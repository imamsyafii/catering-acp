<?php

namespace app\models;
use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;
/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $work_phone
 * @property string $home_phone
 * @property string $mobile_phone
 * @property string $gender
 * @property string $date_of_birth
 * @property string $reg_type
 * @property string $social_id
 * @property string $avatar
 * @property string $username
 * @property string $password
 * @property int $status
 * @property int $username_base
 * @property int $sms
 * @property int $newsletter
 * @property int $age_range_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $email_temp
 * @property int $reputation
 * @property string $auth_key
 */
class Customer extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['gender'], 'string'],
            [['date_of_birth', 'created_at', 'updated_at', 'auth_key', 'age_range_id'], 'safe'],
            [['status', 'username_base', 'sms', 'newsletter', 'reputation'], 'integer'],
            [['first_name', 'last_name', 'email'], 'string', 'max' => 255],
            [['work_phone', 'home_phone', 'mobile_phone', 'social_id'], 'string', 'max' => 50],
            [['reg_type'], 'string', 'max' => 25],
            [['avatar', 'username', 'password'], 'string', 'max' => 150],
            [['email_temp'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'work_phone' => 'Work Phone',
            'home_phone' => 'Home Phone',
            'mobile_phone' => 'Mobile Phone',
            'gender' => 'Gender',
            'date_of_birth' => 'Date Of Birth',
            'reg_type' => 'Reg Type',
            'social_id' => 'Social ID',
            'avatar' => 'Avatar',
            'username' => 'Username',
            'password' => 'Password',
            'status' => 'Status',
            'username_base' => 'Username Base',
            'sms' => 'Sms',
            'newsletter' => 'Newsletter',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'email_temp' => 'Email Temp',
            'reputation' => 'Reputation',
        ];
    }

    public static function registration($vars, $lang = "en") {
        $return = array("error" => 1, "message" => "Register failed.", "details" => []);

        if (isset($vars['reg_type'])) {

            $email = $vars['email'];

            if ($vars['reg_type'] == "email") {
                //email registration section 


                if (isset($vars['user_id'])) {
                    if (!empty($vars['user_id'])) {
                        $account = Customer::find()->where(['id' => $vars['user_id']])->one();

                        if ($account)
                        {
                            $is_new = 0;
                        }
                    }
                }


                if (!isset($account)) {
                    $account = Customer::find()->where(['email' => $email])->one();
                    if ($account) {
                        $response = new APIResponse(1, "The email is already used.", []);
                        return $response->format();
                    }
                    $is_new = 1;
                }
            } else {
                //social media registration section

                $account = Customer::find()->where(['email' => $email])->one();
                if (!$account){
                    if (isset($vars['user_id'])) {
                        if (!empty($vars['user_id'])) {
                            $account = Customer::find()->where(['id' => $vars['user_id']])->one();
                            if ($account){
                                $is_new = 0;
                            }
                        }
                    }
                }

                if ($account) {
                    $is_new = 0;
                    if ($account->reg_type == "email") {
                        //show error user already registered with another method
                        $response =  new APIResponse(1, "The email is already used.", []);
                        return $response->format();
                    }
                } else {
                    $is_new = 1;
                }
            }
        }

        if ($is_new == 1) {
            $account = new Customer();
            $account->auth_key = (new User)->generateAuthKey();
            $account->first_name = isset($vars['first_name']) ? $vars['first_name'] : NULL;
            $account->last_name = isset($vars['last_name']) ? $vars['last_name'] : NULL;
            $account->email = isset($vars['email']) ? $vars['email'] : NULL;
            $account->work_phone = isset($vars['work_phone']) ? $vars['work_phone'] : NULL;
            $account->home_phone = isset($vars['home_phone']) ? $vars['home_phone'] : NULL;
            $account->mobile_phone = isset($vars['mobile_phone']) ? $vars['mobile_phone'] : NULL;
            $account->gender = isset($vars['gender']) ? $vars['gender'] : NULL;
            $account->date_of_birth = isset($vars['date_of_birth']) ? $vars['date_of_birth'] : NULL;

            $account->reg_type = isset($vars['reg_type']) ? $vars['reg_type'] : NULL;
            $account->social_id = isset($vars['social_id']) ? $vars['social_id'] : NULL;
            $account->avatar = isset($vars['avatar']) ? $vars['avatar'] : NULL;
            $account->username = isset($vars['username']) ? $vars['username'] : NULL;
            $account->username_base = isset($vars['username_base']) ? $vars['username_base'] : NULL;

            $account->password = isset($vars['password']) ? $vars['password'] : NULL;
            $account->status = 1;
        } else {
            if(empty($account->auth_key))
            {
                $account->auth_key = (new User)->generateAuthKey();
            }
            $account->first_name = isset($vars['first_name']) ? $vars['first_name'] : $account->first_name;
            $account->last_name = isset($vars['last_name']) ? $vars['last_name'] : $account->last_name;
            $account->email = isset($vars['email']) ? $vars['email'] : $account->email;
            $account->work_phone = isset($vars['work_phone']) ? $vars['work_phone'] : $account->work_phone;
            $account->home_phone = isset($vars['home_phone']) ? $vars['home_phone'] : $account->home_phone;
            $account->mobile_phone = isset($vars['mobile_phone']) ? $vars['mobile_phone'] : $account->mobile_phone;
            $account->gender = isset($vars['gender']) ? $vars['gender'] : $account->gender;
            $account->date_of_birth = isset($vars['date_of_birth']) ? $vars['date_of_birth'] : $account->date_of_birth;
            $account->reg_type = isset($vars['reg_type']) ? $vars['reg_type'] : $account->reg_type;
            $account->social_id = isset($vars['social_id']) ? $vars['social_id'] : $account->social_id;
            if(empty($account->avatar))
                $account->avatar = isset($vars['avatar']) ? $vars['avatar'] : $account->avatar;
            $account->username = isset($vars['username']) ? $vars['username'] : $account->username;
            $account->username_base = isset($vars['username_base']) ? $vars['username_base'] : $account->username_base;
        }



        if ($account->save()) {
            $token = JWTAuthenticator::getToken($account->id);
            $return = Customer::UserDetail(array("user_id" => $account->id, "token"=>(string)$token), $lang);
        }
        return $return;
    }
    
    public static function login($vars, $lang = "en") {
        $account = Customer::find()->where("email=:email and password=:password and reg_type=:reg_type", ['email' => $vars['email'], 'password' => $vars['password'], 'reg_type' => 'email'])->one();
        $email = $vars['email'];
        if ($account) {
            $token = JWTAuthenticator::getToken($account->id);
            $return = Customer::UserDetail(array("user_id" => $account->id, "token"=>(string)$token), $lang);
            $response = new APIResponse(0, 'Success Login', $return['details']);
        } else {
            $checkEmail = Customer::find()->where(['email' => $email])->one();
            if ($checkEmail && $checkEmail->reg_type != 'email') {
                $msg = 'Your email is already registered by using social media';
            } else {
                $msg = 'Wrong Password!';
            }
            $response = new APIResponse(1, ($checkEmail) ? $msg : 'Account Not Found!', array());
        }
        return $response->format();
    }

    public static function UserDetail($vars, $lang = "en") {
        $return = new APIResponse(1, "No User found.", []);

        $account = Customer::find()
                //->select("id, first_name, last_name, email, mobile_phone, gender, date_of_birth, reg_type, social_id, avatar, username,status, username_base, auth_key, created_at, updated_at ")
                ->select("id, first_name, last_name, email, mobile_phone, gender, date_of_birth, reg_type, social_id, avatar")
                ->where(['id' => $vars['user_id']])->asArray()->all();
        $account = General::checkForNull($account);
        if (!empty($account)) {
            
              for($a=0; $a<count($account); $a++)
              {
                  if(isset($vars['token'])){
                    $account[$a]['token'] = $vars['token'];
                  }
              }
             
           $return = new APIResponse(0, "User found.", $account);
            
        }
        
	$response = $return->format();
        return $response;
    } 
    
    public static function forgotPassword($vars, $lang)
    {
        $extrafilter = "";
        if(isset($vars['user_id']))
        {
            $extrafilter .= " and id='$vars[user_id]'";
        }
         $email = $vars['email'];
         $account = Customer::find()->where("email=:email and reg_type=:reg_type $extrafilter", ['email'=>$email, 'reg_type'=>'email'])->one(); 
            //return array($social_id, $registration_type);
            if($account)
            {
                //send email
                
                $link = Url::base(true).'/public/passwordreset?m='.$email.'&i='.$account->id.'&t='.time().'&c='.$account->created_at;
                
                $data = array(
                    'email' => $email,
                    'title' => 'REQUEST PASSWORD RECOVERY',
                    'body' => '',
                );
                
                $value = array('date'=>date('d-m-Y H:i:s'), 'link'=>$link);
		//sending email
                //ApiSyaf::sendEmail($data, $value, 2);
                $return = new APIResponse(0, "Change password request has been sent, check your email", []);
                
                
            }
            else
            {
            $err_mess = ($lang=="ar")?"بريدك الإلكتروني غير مسجل.":'Your email is not registered.';
            
            $return = new APIResponse(1, $err_mess, []);
            }
            
        return $return->format();   
     }
     
    public static function changePassword($vars, $lang)
    {
        $account = Customer::find()->where(['id'=>$vars['user_id']])->one(); 
        if(!$account)
        {
            $err_mess = ($lang=="ar")?"بريدك الإلكتروني غير مسجل.":'Your email is not registered.';
            $return = new APIResponse(1, $err_mess, []);
            return $return->format();
        }
        $email = $account->email;
        if($account->password == ($vars['old_password']))
        {
                $account->password = $vars['new_password'];
                if($account->save())
                {	
                    //sending email
                    $data = array(
                       'email' => $email,
                       'title' => 'Password Changed',
                       'body' => '',
                    );
                    $value = array('date'=>date('d-m-Y H:i:s'));
                    //$send = \app\models\ApiSyaf::sendEmail($data, $value, 9);
                }
        }
        else
        {
            $err_mess = "Old password isn't valid";
            $return = new APIResponse(1, $err_mess, []);
            return $return->format();
        }

        $err_mess = "Password changed successfully";
        $return = new APIResponse(0, $err_mess, []);
        return $return->format();
    }
    
    public static function updateAvatar($vars, $lang){
        $account = Customer::find()->where(['id'=>$vars['user_id']])->one(); 
        
        
        $image = UploadedFile::getInstanceByName('avatar');
        $image_name = "";
        $image_url = "";
        if (!is_null($image)) {
           $upload_image = new \app\models\uploadImage($image, 'avatar');
           $image_name = $upload_image->execute();
           
           $image_url = Url::base(true)."/upload_file/avatar/".$image_name;
           
           $account->avatar = $image_url;
           $account->save();
           
        }
        else{
            $err_mess = "Change avatar failed";
            $return = new APIResponse(1, $err_mess, $response);
            return $return->format();
        }
        
        $response = Customer::find()->where(['id'=>$vars['user_id']])->asArray()->all(); 
        
        $response = General::checkForNull($response);
        $err_mess = "Avatar uploaded successfully";
        $return = new APIResponse(0, $err_mess, $response);
        return $return->format();
    }


    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(['id' => $token->getClaim('uid')]);
    }

    public function getAuthKey() {
        return $this->authKey;
    }

    public function getId() {
        return $this->id;
    }

    public function validateAuthKey($authKey) {
        return false;
    }

}
