<?php
namespace app\models;
use Yii;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of General
 *
 * @author imamsyafii
 */
class ApiSyaf {

    public static function checkParam($table_columns, $input)
    {
        
        $return = array('code'=>0, 'message'=>'');
	//$table_columns = $var;
        foreach($table_columns as $col){
                
                if(!isset($input[$col]))
                {
                            $return['code'] = 1;   
                            if(empty($return['message']))
                            { 
                                $return['message'] = $col." is required";
                            }
                }else
                {
                    $data[$col] = $input[$col];
                }
        }
        return $return;
    }
    
    public static function sendEmail($vars, $arr, $type=0)
    {
        $email = $vars['email'];
        $title = $vars['title'];
        //$body = $vars['body'];
        
        if($type=='1')//forgot password
        {
            $template = file_get_contents("mail_template/template_forgot.html");
        }
        
        switch($type)
         {
	         case 1: //forgot password
	         	$template = file_get_contents("mail_template/template_forgot.html");
	         break;
                 
             case 2: //forgot password
	         	$template = file_get_contents("mail_template/template_forgotrequest.html");
	         break;
             
             
             case 3: //voucher
	         	$template = file_get_contents("mail_template/template_sendvoucher.html");
	         break;
	         
	         case 4: //Success Registration
	         	$template = file_get_contents("mail_template/template_successregistration.html");
	         break;
	         
	         case 5: //License Expired
	         	$template = file_get_contents("mail_template/template_license_expiry.html");
	         break;
	         
	         case 6: //Order Confirmation
	         	$template = file_get_contents("mail_template/template_confirmation.html");
	         break;
	         
	         case 7: //voucher
	         	$template = file_get_contents("mail_template/template_sendcredit.html");
	         break;
	         
	         
	         case 8: //Catering Order Invoice
	         	$template = file_get_contents("mail_template/template_cateringinvoice.html");
	         break;
	         
	         case 9: //change password
	         	$template = file_get_contents("mail_template/template_changepassword.html");
	         break;
	         
	         default:
	         
	         break;
	         
         }
         $social = array(array('common_value'=>'kang.imamsyafii'),array('common_value'=>'kang.imamsyafii'),array('common_value'=>'kang.imamsyafii'));
         $social_array = array('facebook_link'=>'https://www.facebook.com/'.$social[0]['common_value'],
                                'instagram_link'=>'https://www.instagram.com/'.$social[1]['common_value'],
                                'twitter_link'=>'https://www.twitter.com/'.$social[2]['common_value'],	
		);
         if($type>0)
         {
         	
         	$arr = array_merge($arr, $social_array);
	         
	         $str = $template;
	         if(isset($arr))
                 {
			 foreach($arr as $key => $value) {
				$str = str_replace("{{".$key."}}", $value, $str);
			}
                 }
			$body = $str;
         }
        
        return Yii::$app->mail->compose()
                ->setTo($email)
                ->setFrom(['noreply@oor.com.kw' => 'DOCTOR'])
                ->setSubject($title)
                ->setHtmlBody($body)
                
                ->send();
    }
    
    public static function generateSerial($part=4, $length=5)
    {
        $tokens = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

        $serial = '';
        
        for ($i = 0; $i < $part; $i++) {
            for ($j = 0; $j < $length; $j++) {
                $serial .= $tokens[rand(0, 35)];
            }

            if ($i < ($part-1)) {
                $serial .= '-';
            }
        }
        
        return $serial;
    }

}
