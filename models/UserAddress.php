<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_address".
 *
 * @property string $id
 * @property int $user_id
 * @property int $address_id
 * @property int $type
 * @property int $status
 */
class UserAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'address_id', 'type', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'address_id' => 'Address ID',
            'type' => 'Type',
            'status' => 'Status',
        ];
    }
}
