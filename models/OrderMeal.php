<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_meal".
 *
 * @property int $id
 * @property string|null $order_reference
 * @property string|null $date_requested
 * @property int|null $menu_id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $custom_menu
 */
class OrderMeal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_meal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_requested', 'created_at', 'updated_at'], 'safe'],
            [['menu_id', 'custom_menu'], 'integer'],
            [['order_reference'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_reference' => 'Order Reference',
            'date_requested' => 'Date Requested',
            'menu_id' => 'Menu ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'custom_menu' => 'Custom Menu',
        ];
    }
}
