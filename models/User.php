<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $confirmation_token
 * @property int $status
 * @property int $superadmin
 * @property int $created_at
 * @property int $updated_at
 * @property int $lastpass_changed
 * @property string $last_wrong_login
 * @property int $wrong_count
 * @property string $registration_ip
 * @property string $bind_to_ip
 * @property string $email
 * @property int $email_confirmed
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthItem[] $itemNames
 * @property UserVisitLog[] $userVisitLogs
 */
class User extends \yii\db\ActiveRecord
{
    public $fullname, $phone_number, $password, $repeat_password, $role;
    public $shop_id, $gridRoleSearch;
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['username', 'auth_key', 'password_hash', 'created_at', 'updated_at', 'lastpass_changed', 'wrong_count'], 'required'],
           
            [['last_wrong_login', 'fullname', 'phone_number', 'email_confirmed', 'role', 'shop_id','gridRoleSearch'], 'safe'],
            //[['fullname', 'phone_number', 'role' ,'email'],'required'],
            [['email'],'required'],
            [['username', 'password_hash', 'confirmation_token', 'bind_to_ip'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['registration_ip'], 'string', 'max' => 15],
            [['email'], 'string', 'max' => 128],
            ['username', 'unique', 'targetAttribute' => ['username'], 'message' => 'Username is already used.'],

            //[['fullname', 'phone_number','password', 'role', 'repeat_password', 'username'], 'required'],
            [['password', 'repeat_password'], 'string', 'max'=>255],
            [['password', 'repeat_password'], 'trim'],

            //['password', 'match', 'pattern' => '/^((?!'.$filterText.').)*$/i', 'message' => 'Your password could not contain your username'],
            ['password', 'match', 'pattern' => '/^[^\s]*$/','message' => 'Whitespace is not allowed'],
            ['password', 'match', 'pattern' => Yii::$app->getModule('user-management')->passwordRegexp, 'message' => Yii::$app->getModule('user-management')->passwordRequiryTitle],

            ['repeat_password', 'compare', 'compareAttribute'=>'password'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'confirmation_token' => 'Confirmation Token',
            'status' => 'Status',
            'superadmin' => 'Superadmin',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'lastpass_changed' => 'Lastpass Changed',
            'last_wrong_login' => 'Last Wrong Login',
            'wrong_count' => 'Wrong Count',
            'registration_ip' => 'Registration Ip',
            'bind_to_ip' => 'Bind To Ip',
            'email' => 'Email',
            'email_confirmed' => 'Email Confirmed',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemNames()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])->viaTable('auth_assignment', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVisitLogs()
    {
        return $this->hasMany(UserVisitLog::className(), ['user_id' => 'id']);
    }
    
    public static function createUser($vars){
        $model = !isset($vars['user_id'])?new User():User::find()->where(['id'=>$vars['user_id']])->one();
            $ipaddress = '';
            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ipaddress = $_SERVER['REMOTE_ADDR'];
            //print_r($model);
            //print_r($vars['email']);
            $model->email = $vars['email'];
            $model->registration_ip = $ipaddress;
            $model->auth_key = Yii::$app->getSecurity()->generateRandomString();
            
            if(!empty($vars['password']))
            $model->password_hash = (Yii::$app->security->generatePasswordHash($vars['password']));
            $model->created_at = time();
            $model->updated_at = time();
            $model->username = $vars['username'];
            if($model->save())
            {
                $detail = !isset($vars['user_id'])?new UserDetail():UserDetail::find()->where(['id'=>$vars['user_id']])->one();
                $detail->id = $model->id;
                $detail->fullname = isset($vars['fullname'])?$vars['fullname']:NULL;
                $detail->email = $model->email;
                $detail->phone_number = isset($vars['phone_number'])?$vars['phone_number']:NULL;
                $detail->shop_id = isset($vars['shop_id'])?$vars['shop_id']:NULL;
                
                $detail->save();
                
                $role = !isset($vars['user_id'])?new \app\models\AuthAssignment():AuthAssignment::find()->where(['user_id'=>$vars['user_id']])->one();;
                $role->item_name = $vars['role'];
                $role->user_id = $model->id;
                $role->created_at = time();
                $role->save();
            //return $this->redirect(['view', 'id' => $model->id]);
                return $vars;
            }
            else
            {
                return ($model->getErrors());
                exit;
            }
    }
}
