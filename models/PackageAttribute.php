<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "package_attribute".
 *
 * @property int $id
 * @property string|null $title_en
 * @property string|null $title_ar
 * @property string|null $description_en
 * @property string|null $description_ar
 * @property string|null $image
 * @property int|null $status
 */
class PackageAttribute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'package_attribute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description_en', 'description_ar'], 'string'],
            [['status'], 'integer'],
            [['title_en', 'title_ar', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_en' => 'Title En',
            'title_ar' => 'Title Ar',
            'description_en' => 'Description En',
            'description_ar' => 'Description Ar',
            'image' => 'Image',
            'status' => 'Status',
        ];
    }
}
