<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\Knet;

/**
 * Description of KnetResponseHandler
 *
 * @author imam
 */

/*
$request = Yii::$app->request;

// returns all parameters
$params = $request->bodyParams;

// returns the parameter "id"
$param = $request->getBodyParam('id');
 $request = Yii::$app->request;

//if ($request->isAjax) { //the request is an AJAX request  }
//if ($request->isGet)  { // the request method is GET }
//if ($request->isPost) { // the request method is POST }
//if ($request->isPut)  { // the request method is PUT }
 *  */
class KnetResponseHandler extends KnetClient
{
    private $result = [];
    private $error = null;
    private $error_code = '';

    public function __construct()
    {
        if (request()->exists('trandata')) {
            foreach ($this->decryptedData() as $datum) {
                $temp = explode('=', $datum);
                if (isset($temp[1])) {
                    if ($temp[0] == 'result') {
                        $temp[1] = implode(' ', explode('+', $temp[1]));
                        $this->result['paid'] = $temp[1] == 'CAPTURED';
                    }
                    $this->result[Str::snake($temp[0])] = $temp[1];
                }
            }
        } else {
            $this->result['error_text'] = request('ErrorText');
            $this->result['error'] = request('Error');
            $this->result['paymentid'] = request('paymentid');
            $this->result['avr'] = request('avr');
            $this->result['result'] = 'FAILED';

            $this->error = request('ErrorText');
            $this->error_code = request('Error');
        }

        return $this;
    }

    public function __toString()
    {
        return json_encode($this->result);
    }

    public function toArray()
    {
        return $this->result;
    }

    public function hasErrors()
    {
        return !is_null($this->error);
    }

    public function error()
    {
        return $this->hasErrors() ? $this->error : null;
    }

    private function decrypt($tranData)
    {
        return $this->decryptAES($tranData, config('knet.resource_key'));
    }

    private function decryptedData()
    {
        $tranData = request('trandata');
        return explode('&', $this->decrypt($tranData));
    }

    public function __get($name)
    {
        return $this->result[$name];
    }
}
