<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\Knet;

/**
 * Description of KnetException
 *
 * @author imam
 */
class KnetException extends \Exception
{
    /**
     * Create a new PaymentFailure instance.
     *
     * @return self
     */
    public static function unauthorizedReferer()
    {
        return new self(
            'The request attempt failed because of an invalid request referer.'
        );
    }

    /**
     * Create a new PaymentFailure instance.
     *
     * @return self
     */
    public static function missingResourceKey()
    {
        return new self(
            'Sorry, you are missing resource key, you can\'t continue without it.'
        );
    }

    /**
     * Create a new PaymentFailure instance.
     *
     * @return self
     */
    public static function missingAmount()
    {
        return new self(
            'Sorry, you are forget to set amount, you can\'t continue without it.'
        );
    }

    /**
     * Create a new PaymentFailure instance.
     *
     * @return self
     */
    public static function missingTrackId()
    {
        return new self(
            'Sorry, you are forget to set Track Id, you can\'t continue without it.'
        );
    }
}
