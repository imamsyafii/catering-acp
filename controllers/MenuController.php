<?php

namespace app\controllers;

use Yii;
use app\models\Menu;
use app\models\MenuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Menu();

        if ($model->load(Yii::$app->request->post())) {
            $model->package_id = isset($_GET['pid'])?$_GET['pid']:0;
            
            
            $image = UploadedFile::getInstance($model, 'image_src');
           
            
            if (!is_null($image)) {
               $upload_image = new \app\models\uploadImage($image, 'menu');
               $image_name = $upload_image->execute();
               
               if(!empty($image_name)){
                   $model->image = $image_name;
               }
            }
            $model->save();
            
            if(!empty($_POST['attr'])){
                foreach($_POST['attr'] as $attr_key=>$attr_val){
                    $attr = \app\models\MenuAttributeValue::find()
                            ->where("menu_id=:mid and attribute_id=:aid",['mid'=>$model->id, 'aid'=>$attr_key])
                            ->one();
                    if(empty($attr)){
                        $attr = new \app\models\MenuAttributeValue();
                    }
                    
                    $attr->attribute_id = $attr_key;
                    $attr->menu_id = $model->id;
                    $attr->value = $attr_val;
                    $attr->status = 1;
                    $attr->save();
                }
            }
            
            return $this->redirect(['package/view', 'id' => $model->package_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image_before = $model->image;
        if ($model->load(Yii::$app->request->post())) {
            $model->package_id = isset($_GET['pid'])?$_GET['pid']:0;
            $image = UploadedFile::getInstance($model, 'image_src');
            if (!is_null($image)) {
               $upload_image = new \app\models\uploadImage($image, 'menu');
               $image_name = $upload_image->execute();
               
               if(!empty($image_name)){
                   $model->image = $image_name;
               }
            }
            else
            {
	            $model->image = $image_before;
            }
            
            $model->save();
            
            //print_r($_POST['attr']);
            if(!empty($_POST['attr'])){
                foreach($_POST['attr'] as $attr_key=>$attr_val){
                    $attr = \app\models\MenuAttributeValue::find()
                            ->where("menu_id=:mid and attribute_id=:aid",['mid'=>$model->id, 'aid'=>$attr_key])
                            ->one();
                    if(empty($attr)){
                        $attr = new \app\models\MenuAttributeValue();
                    }
                    
                    $attr->attribute_id = $attr_key;
                    $attr->menu_id = $model->id;
                    $attr->value = $attr_val;
                    $attr->status = 1;
                    $attr->save();
                }
            }
            //exit;
            return $this->redirect(['package/view', 'id' => $model->package_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
	    $checkData = $this->findModel($id);
	    if(!empty($checkData)){
        $this->findModel($id)->delete();
	    }
	    
		return $this->redirect(['package/view', 'id' => $checkData->package_id]);

//        return $this->redirect(['index']);
    }
    
    public function actionRemove($id)
    {
	    $checkData = $this->findModel($id);
	    if(!empty($checkData)){
        $this->findModel($id)->delete();
	    }
	    
		return $this->redirect(['package/view', 'id' => $checkData->package_id]);

//        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
