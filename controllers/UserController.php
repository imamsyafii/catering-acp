<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserDetail;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\User as UserHelper;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $detail = \app\models\UserDetail::find()->where(['id'=>$model->id])->one();
        $role = \app\models\AuthAssignment::find()->where(['user_id'=>$model->id])->one();
        return $this->render('view', [
            'model' => $model,
            'detail' => $detail,
            'role' => $role,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        $model = new User();

        if ($model->load(Yii::$app->request->post()) ) {
            //print_r($model);
            //exit;
            
            
            $ipaddress = '';
            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ipaddress = $_SERVER['REMOTE_ADDR'];

            $model->registration_ip = $ipaddress;
            $model->auth_key = Yii::$app->getSecurity()->generateRandomString();
            $model->password_hash = (Yii::$app->security->generatePasswordHash($model->password));
            $model->created_at = time();
            $model->updated_at = time();
            if($model->save())
            {
            	$detail = UserDetail::find()->where(['id'=>$model->id])->one();
            	if(!$detail)
                $detail = new UserDetail();
                $detail->id = $model->id;
                $detail->fullname = $model->fullname;
                $detail->email = $model->email;
                $detail->phone_number = $model->phone_number;
                $detail->shop_id = $model->shop_id;
                
                $detail->save();
                
                $role = new \app\models\AuthAssignment();
                $role->item_name = $model->role;
                $role->user_id = $model->id;
                $role->created_at = time();
                $role->save();
            return $this->redirect(['view', 'id' => $model->id]);
            }
            else
            {
                return $this->render('create', [
                'model' => $model,
                'role'=>[],
                'detail' => [],
            ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'role'=>[],
                'detail' => [],
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model  = $this->findModel($id);
        $detail = \app\models\UserDetail::find()->where(['id'=>$model->id])->one();
        $role   = \app\models\AuthAssignment::find()->where(['user_id'=>$model->id])->one();
        $oldmodel = $model;
        
        
        
        $model->shop_id = !empty($detail->shop_id)?$detail->shop_id:null;
        if ($model->load(Yii::$app->request->post()) )
        {
            if(empty($model->password))
            {
                $model->password_hash = $oldmodel->password_hash;
            }
            else
            {
                $model->password_hash = (Yii::$app->security->generatePasswordHash($model->password));
            }
            if($model->save()) {
                
                if(empty($detail))
                $detail = new UserDetail();
                $detail->id = $model->id;
                $detail->fullname = $model->fullname;
                $detail->email = $model->email;
                $detail->phone_number = $model->phone_number;
                $detail->shop_id = $model->shop_id;
                $detail->save();
                
                if(empty($role))
                $role = new \app\models\AuthAssignment();
                $role->item_name = $model->role;
                $role->user_id = $model->id;
                $role->created_at = time();
                $role->save();
                
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'role' => $role,
                'detail'=> $detail,
            ]);
        }
    }
    
    public function actionProfile(){
        $session = Yii::$app->session;
        
        $user_access = $session->get('user_access');
        $user_roles = $session->get('user_roles');
        $user_detail = $session->get('user_detail');
        
        $user_identity = (Yii::$app->user->identity);
        print_r($user_identity);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
