<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ApiSyaf;
use app\models\ContactForm;
use yii\helpers\Json;
use yii\helpers\Url;
use sizeg\jwt\JwtHttpBearerAuth;
//Yii::$app->controller->enableCsrfValidation = false;

date_default_timezone_set('Asia/Kuwait');
header('Content-type: application/json');
class ApiController extends Controller
{
    public $enableCsrfValidation = false;
    
    public function actions()
    {
//        return [
//            'error' => [
//                'class' => 'yii\web\ErrorAction',
//            ],
//            'captcha' => [
//                'class' => 'yii\captcha\CaptchaAction',
//                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
//            ],
//        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
            'optional' => [
                'index','list', 'create'
            ],

        ];
    //    $behaviors['verbs'] = [ 
    //            'class' => VerbFilter::className(), 
    //            'actions' => [ 'test' => ['POST'], ], 
    //            ];
        return $behaviors;
    }
     public function beforeAction($action)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //ob_start();
        return parent::beforeAction($action);
    }
    
    public function actionIndex()
    {
        return (array(1,2,3,4));
    }
    
    public function actionList() {
    	//echo $_GET['model'];
        //$this->_checkAuth();
        switch ($_GET['model']) {    
            case 'category': //it's used for listing men and women with also the desc
                return \app\models\Category::getAllCategories($_GET);
            break;
        
            case 'package': //it's used for listing package
                return \app\models\Package::getAllPackages($_GET);
            break;
        
            case 'package_detail': //it's used to get details of package
                $return['error'] = 0;
                $return['message'] = '';
                $var = array(
                    'package_id',
                );
                $param = Yii::$app->request->get();
                $check = ApiSyaf::checkParam($var, $param);
                if ($check['code'] == '0') {
                    $return['message'] = 'Success';

                    $headers = Yii::$app->request->headers;
                    $lang = $headers['lang'];

                    $return = \app\models\Package::getPackageDetail($param);
                } else {
                    $return = $check;
                }
                return $return;
                break;
                
            break;
        
            case 'forgot_password':
                $return['error'] = 0;
                $return['message'] = '';
                $var = array(
                    'email',
                );
                $post = Yii::$app->request->get();
                $check = ApiSyaf::checkParam($var, $post);
                if ($check['code'] == '0') {
                    $return['message'] = 'Success';

                    $headers = Yii::$app->request->headers;
                    $lang = $headers['lang'];

                    $return = \app\models\Customer::forgotPassword($post, $lang);
                } else {
                    $return = $check;
                }
                return $return;
                break;
            
            case 'test2':
                return \app\models\Package::doOrder([]);
                break;
            default:
               return (array('code' => 1, 'mesage'=>'No Model defined'));
                break;
            
        }
    }
    
    public function actionCreate() {
        //$this->_checkAuth();
        //echo "testt";
       switch ($_GET['model']) {
            case 'add_device':
                $var = array (

                    'app_identifier',
                    'app_version',
                    'device_type',
                    'device_token',
                    'timezone'
               );
	
                $request = Yii::$app->request;
                $post = $request->post();
                $check = ApiSyaf::checkParam($var, $post);

                if($check['code']=='0')
                {
                   $code = 0;
                   $message = 'Success';
                   $post = Yii::$app->request->post();
                   $headers = Yii::$app->request->headers;
                   
                   $lang = $headers['lang'];
                   
                   $details = \app\models\Devices::addDevice($post, $lang);

                }
                else {
                    $code = 1;
                    $message = $check['message'];
                    $details = [];
                }
                
               
                $return = new \app\models\APIResponse($code, $message, $details);
                return $return->format();
                break;
            case 'registration':
                $var = array(
                    //'fullname',
                    // 'email',
                    // 'regtype',
                    'is_official',
                        //'username',
                );
                $headers = Yii::$app->request->headers;
                $lang = $headers['lang'];

                $post = Yii::$app->request->post();

                if (isset($post['is_official']))
                {
                    if ($post['is_official'] == '1') {
                        //$var[] = 'user_id';
                        $var[] = 'first_name';
                        $var[] = 'last_name';
                        $var[] = 'email';
                        //$var[] = 'mobile_number';
                        $var[] = 'reg_type';
                    }
                }
                if (isset($post['reg_type'])) {
                    if ($post['reg_type'] != 'email') {
                        $var[] = 'social_id';
                    }
                }

                $check = ApiSyaf::checkParam($var, $post);

                if ($check['code'] == '0') {
                    //$country_id = $_POST['country_id'];
                    $return['error'] = 0;
                    $return['message'] = 'success';
                    $request = \Yii::$app->request;

                    $email_temp = NULL;
                    $headers = Yii::$app->request->headers;
                    $lang = $headers['lang'];
                    $post_param = $post;

                    if ($_POST['is_official'] == '0') {
                        if (!empty($email)) {
                            $checkByEmail = \app\models\Customer::find()
                                    ->where("Email=:email", ['email' => $email])
                                    ->asArray()
                                    ->all();
                            if (count($checkByEmail) > 0) {

                                $err_mess = ($lang == "ar") ? "بريدك الإلكتروني مسجل بالفعل. الرجاء تسجيل الدخول بدلا من ذلك." : "Your email is already registered. Please login instead.";
                                $return = array('code' => 1,
                                    'message' => $err_mess, //"Your email is already registered. Please login instead.",
                                    'login' => 1,
                                    'details' => array()
                                );

                                return $return;
                            } else {
                                $email_temp = $email;
                                $post_param['email_temp'] = $email;
                            }
                        }
                        $randomize = time() . rand(1000, 9999);
                        $fullname = !empty($fullname) ? $fullname : 'Unofficial ' . $randomize;
                        $email = 'unofficial' . $randomize . '@catunofficial.com';
                        //$registration_type = 'none';
                        //$status = '0';
                        $post_param['email'] = $email;

                    }

                    $return = \app\models\Customer::registration($post_param, $lang);
                } else {
                    $return = $check;
                }
                return $return;
                break;
            case 'login':
                $var = array(
                    'email',
                    'password',
                );

                $post = Yii::$app->request->post();
                $check = ApiSyaf::checkParam($var, $post);

                if ($check['code'] == '0') {
                    $return = \app\models\Customer::login($post);
                } else {
                    $return = $check;
                }
                return $return;
                break;
            case 'test':
                return ($_POST);
                break;
            
            default :
                return (array('code' => 1, 'mesage'=>'No Model defined'));
                break;
            
        } 
    }
    
    
    
    
    
    
    
    
    
    public function actionPersonallist() {
    	//echo $_GET['model'];
        //$this->_checkAuth();
        switch ($_GET['model']) {    
            case 'user_detail':
                $identity = \Yii::$app->user->identity;
                    if(!empty($identity))
                    {
                            if($identity->role=="Customer")
                            {
                               $id = $identity->id;
                               $param = array("user_id" => $id);
                               $headers = Yii::$app->request->headers;
                               $lang = $headers['lang'];
                               $return = \app\models\Customer::UserDetail($param, $lang);
                            }
                            else
                            {
                               return $identity;
                            }
                    }
               return $return;
                break;
            case 'address':
                $headers = Yii::$app->request->headers;
                $lang = $headers['lang'];

                $identity = \Yii::$app->user->identity;
                if(!empty($identity))
                {
                    if($identity->role=="Customer")
                    {
                       $id = $identity->id;
                    }
                } 
                $param = array("user_id" => $id);
                $return = \app\models\Address::CustAddress($param, $lang);
                return $return;
                break;
            case 'set_default_address':
                $return['error'] = 0;
                $return['message'] = '';
                $var = array(
                    'address_id'
                );

                $get = Yii::$app->request;
                $param = $get->get();
                
                //$id = $
                //$param['address_id'] = $id;
                $check = ApiSyaf::checkParam($var, $param);
                if ($check['code'] == '0') {
                    $return['message'] = 'Success';

                    $headers = Yii::$app->request->headers;
                    $lang = $headers['lang'];
                    $identity = \Yii::$app->user->identity;
                    if(!empty($identity))
                    {
                        if($identity->role=="Customer")
                        {
                           $id = $identity->id;
                           $param['user_id'] = $id;
                        }
                    } 

                    $return = \app\models\Address::setDefaultAddress($param, $lang);
                    if (empty($return['details'])) {
                        $return['code'] = 1;
                        $return['message'] = 'Update Failed.';
                    } else {

                    }
                } else {
                    $return = $check;
                }
                return $return;
                break;
                
            case 'delete_address':
                $return['error'] = 0;
                $return['message'] = '';
                $var = array(
                    'address_id'
                );

                $get = Yii::$app->request;
                $param = $get->get();
                
                //$id = $
                //$param['address_id'] = $id;
                $check = ApiSyaf::checkParam($var, $param);
                if ($check['code'] == '0') {
                $headers = Yii::$app->request->headers;
                $lang = $headers['lang'];

                $identity = \Yii::$app->user->identity;
                if(!empty($identity))
                {
                    if($identity->role=="Customer")
                    {
                       $user_id = $identity->id;
                    }
                } 
                
                $param = array("user_id" => $user_id, "address_id" => $param['address_id']);
                $return = \app\models\Address::DeleteAddress($param, $lang);
                }else
                {
                    $return = $check;
                }
                return $return;
                break;
            case 'test2':
               return ($_POST);
                break;
            default:
               return (array('code' => 1, 'mesage'=>'No Model defined'));
                break;
            
        }
    }
    
    public function actionPersonalcreate() {
        //$this->_checkAuth();
        //echo "testt";
       switch ($_GET['model']) {
           case 'update_address':
                $return['code'] = 0;
                $return['message'] = '';
                $var = array(
                    'is_new',
                    'address_name',
                    'address_type',
                    //'area',
                    'block',
                    'street',
                    'building',
                    'floor',
                    //'user_id',
                    'city_id',
                );

                $post = Yii::$app->request->post();

                if (isset($post['is_new']))
                {
                    if ($post['is_new'] == '0') {
                        $var[] = 'address_id';
                    }
                }
                $check = ApiSyaf::checkParam($var, $post);
                if ($check['code'] == '0') {
                    $return['message'] = 'Success';

                    $headers = Yii::$app->request->headers;
                    $lang = $headers['lang'];
                    $identity = \Yii::$app->user->identity;
                    if(!empty($identity))
                    {
                        if($identity->role=="Customer")
                        {
                           $id = $identity->id;
                           $post['user_id'] = $id;
                        }
                    } 

                    $return = \app\models\Address::updateAddress($post, $lang);
                    if (empty($return['details'])) {
                        $return['code'] = 1;
                        $return['message'] = 'Update Failed.';
                    } else {

                    }
                } else {
                    $return = $check;
                }
                return $return;
                break;
                
            case 'test':
                return ($_POST);
                break;
            
            default :
                return (array('code' => 1, 'mesage'=>'No Model defined'));
                break;
            
        } 
    }


}
