<?php

namespace app\controllers;

use Yii;
use app\models\Package;
use app\models\PackageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\helpers\Url;

/**
 * PackageController implements the CRUD actions for Package model.
 */
class KnetController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    
    public function actionExecute()
    {
        $options = [
            'amt' => 10,
            'trackid'=>date("YmdHis"). rand(1000, 9999)
        ];
        
        
        $config = Yii::$app->params['knet'];
        $knet = new \app\models\Knet\Knet($options, $config);
        print_r($knet->url());exit;
        return $this->redirect(Url::to('https://www.example.com'));
    }
    
    public function actionResponse()
    {
        $options = [
            'amt' => 10,
            'trackid'=>date("YmdHis"). rand(1000, 9999)
        ];
        
        
        $config = Yii::$app->params['knet'];
        $knet = new \app\models\Knet\Knet($options, $config);
        print_r($knet->url());exit;
        return $this->redirect(Url::to('https://www.example.com'));
    }

    /**
     * Finds the Package model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Package the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Package::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
