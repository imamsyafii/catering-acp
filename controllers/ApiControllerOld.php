<?php
namespace app\controllers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use sizeg\jwt\JwtHttpBearerAuth;
use yii\helpers\Json;

date_default_timezone_set('Asia/Kuwait');
header('Content-type: application/json');
class ApiController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Commons';
    
public function beforeAction($action)
{
    $this->enableCsrfValidation = false;

    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    ob_start();
    //return [];
    return parent::beforeAction($action);

}
public function actions()
{
    $actions = parent::actions();
    unset($actions['delete'], $actions['create'], $actions['update'], $actions['index']);
    //$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
    return $actions;
    
}
public function actions22()
{
    return [
        'error' => [
            'class' => 'yii\web\ErrorAction',
        ],
    ];
}

public function behaviors()
{
    $behaviors = parent::behaviors();
    $behaviors['authenticator'] = [
        'class' => JwtHttpBearerAuth::class,
        'optional' => [
            'index','test'
        ],
         
    ];
//    $behaviors['verbs'] = [ 
//            'class' => VerbFilter::className(), 
//            'actions' => [ 'test' => ['POST'], ], 
//            ];
    return $behaviors;
}
public function actionIndex(){
    return [1234,12];
}

public function actionTest()
{
    
    return $_POST;
}

protected function verbs()
{
   return [
   // 	'test' => ['POST'],  
   ];
}


}
