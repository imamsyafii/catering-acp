<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgeRange */

$this->title = 'Create Age Range';
$this->params['breadcrumbs'][] = ['label' => 'Age Ranges', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="age-range-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
