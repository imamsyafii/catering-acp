<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title_en',
            'title_ar',
            'description_en:ntext',
            'description_ar:ntext',
            //'image',
            'package_id',
            //'status',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function ($model) { 
	               $return = !empty($model->image)? "<img src='".Url::base(true).'/upload_file/brand/'.$model->image."' class='img img-responsive' style='max-width:200px'>":'';
                   return $return;
                },
              ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->status=='1')?'Active':'Inactive';
                }
            ]
        ],
    ]) ?>

</div>
