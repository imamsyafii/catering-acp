<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;


use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_ar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_en')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description_ar')->textarea(['rows' => 6]) ?>
    <?=(!empty($model->image))?"<img src='".Url::base(true).'/upload_file/menu/'.$model->image."' class='img img-responsive' style='max-width:200px'>":''?>
    <?= $form->field($model, 'image_src')->widget(FileInput::classname(), [
                          'options' => ['accept' => 'image/*'],
                           'pluginOptions'=>['allowedFileExtensions'=>['jpeg','jpg','gif','png'],'showUpload' => false,'showPreview' => false,],
                      ]);   ?>

   

    <?= $form->field($model, 'status')->dropDownList([ '0'=>'Inactive', '1'=>'Active', ], ['prompt' => '-Select Status-']) ?>
    <?php
        $attributes = \app\models\MenuAttribute::find()
                ->asArray()
                ->all();
        
        
        
        //print_r($attributes);
        if(!empty($attributes))
        {
        echo "<h3>Attributes</h3>"
            . "<table class='table table-bordered'>";
        foreach($attributes as $att_key=>$att_val){
            $value = \app\models\MenuAttributeValue::find()
                    ->where("menu_id=:mid and attribute_id=:aid", ['mid'=>$model->id, 'aid'=>$att_val['id']])
                    ->asArray()
                    ->one();
            echo "<tr><td>".$att_val['title_en']."</td><td><input class='form-control' name='attr[$att_val[id]]' value='$value[value]'></td></tr>";
        }
        echo "</table>";
        
        }
    ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
