<?php

/* @var $this yii\web\View */

$this->title = 'HEALTH STUDIO';

$category = Yii::$app->db->createCommand("select count(*) as `count` from category;")->queryScalar();
$package = Yii::$app->db->createCommand("select count(*) as `count` from package;")->queryScalar();
$orders = Yii::$app->db->createCommand("select count(*) as `count` from orders;")->queryScalar();
$customer = Yii::$app->db->createCommand("select count(*) as `count` from customer;")->queryScalar();
?>
<div class="site-index">
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-tags"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Category</span>
              <span class="info-box-number"><?=$category?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    Categories
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-check"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Package</span>
              <span class="info-box-number"><?=$package?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    Packages
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-cart-arrow-down"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Orders</span>
              <span class="info-box-number"><?=$orders?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    Orders made
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Customers</span>
              <span class="info-box-number"><?=$customer?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    Registered Users
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
    
    
    <div class="row">
        <div class="col-lg-7">
            <div class="box box-solid box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Today's Orders</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body text-center">
                  

                  
                </div>
                <!-- /.box-body -->
              </div>
        </div>
        <div class="col-lg-5">
            <div class="box box-solid box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Next Week Menu</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body text-center">
                 <?php
                    $monday_nextweek =  date("Y-m-d", strtotime('monday next week'));   
                    $friday_nextweek = date("Y-m-d", strtotime('+4 day', strtotime($monday_nextweek)));   
                    echo '<b>'.$monday_nextweek.' - '.$friday_nextweek.'</b>';
                    
                    $menu_availability = \app\models\MenuAvailability::find()
                            ->select("menu_availability.date_from,menu_availability.date_to,"
                                    . "P.title_en as package,"
                                    . "M.title_en as menu ")
                            ->leftJoin('package P','P.id=menu_availability.package_id')
                            ->leftJoin('menu M','M.id=menu_availability.menu_id')
                            ->where("(menu_availability.date_from>='$monday_nextweek' and menu_availability.date_from<='$friday_nextweek' ) or (menu_availability.date_to>='$monday_nextweek' and menu_availability.date_to<='$friday_nextweek' )"
                                    . " or ('$monday_nextweek' >= menu_availability.date_from and '$monday_nextweek' <= menu_availability.date_to) or ('$friday_nextweek' >= menu_availability.date_from and '$friday_nextweek' <= menu_availability.date_to)  ")
                            ->asArray()
                            ->all();
                 ?>
                    <br/>
                    <table class="table table-bordered table-hover dataTable">
                        <thead><tr>
                            <td>No</td>
                            <td>From</td>
                            <td>To</td>
                            <td>Package</td>
                            <td>Menu</td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php for($a=0; $a<count($menu_availability); $a++){
                            ?>
                        <tr>
                            <td><?=$a+1?></td>
                            <td><?=$menu_availability[$a]['date_from']?></td>
                            <td><?=$menu_availability[$a]['date_to']?></td>
                            <td><?=$menu_availability[$a]['package']?></td>
                            <td><?=$menu_availability[$a]['menu']?></td>
                        </tr>
                           <?php 
                        }?>
                        </tbody>
                        
                    </table>
                  
                </div>
                <!-- /.box-body -->
              </div>
            
        </div>
    </div>
    
</div>
