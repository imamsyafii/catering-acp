<?php

use yii\helpers\Html;

use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_ar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_en')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description_ar')->textarea(['rows' => 6]) ?>
    <?php 
    if($model->isNewRecord){}
    else{
        if(!empty($model->image))
        {
            ?>
    <img src="<?= yii\helpers\Url::base(true).'/uploaded_file/category/'. $model->image?>" width="100px"/>
            <?php
        }
    }
    ?>
    <?= $form->field($model, 'image_src')->widget(FileInput::classname(), [
                          'options' => ['accept' => 'image/*'],
                           'pluginOptions'=>['allowedFileExtensions'=>['jpeg','jpg','gif','png'],'showUpload' => false,'showPreview' => false,],
                      ]);   ?>

    <?= $form->field($model, 'status')->dropDownList([ '0'=>'Inactive', '1'=>'Active', ], ['prompt' => '-Select Status-']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
