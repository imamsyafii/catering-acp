<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = $model->first_name.' '.$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="customer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php // Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
	            'attribute' => 'first_name',
	            'label' => 'Full Name'
            ],
            //'last_name',
            'email:email',
            //'work_phone',
            //'home_phone',
            'mobile_phone',
            //'gender',
            [
                'attribute' => 'gender',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->gender=='f')?'Female':'Male';
                }
            ],
                    [
                'attribute' => 'username',
                'label'=>'Age Range'
            ],
//            'date_of_birth',
            //'reg_type',
            [
                'attribute' => 'reg_type',
                'format' => 'raw',
                'value' => function ($model) {
                    return strtoupper($model->reg_type);
                }
            ],
            //'social_id',
            //'avatar',
            //'username',
            //'password',
/*
            [
                'attribute' => 'newsletter',
                'format' => 'raw',
                'label'=>'Subscribed',
                'value' => function ($model) {
                    return ($model->newsletter=='1')?'Yes':'No';
                }
            ],
*/
                    
            //'status',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->status=='1')?'Active':'Inactive';
                }
            ],
            //'username_base',
            //'sms',
            //'newsletter',
            
            'created_at',
            'updated_at',
            //'email_temp:email',
            //'reputation',
        ],
    ]) ?>
    
    <h3>Addresses:</h3>
    <?php
    /*
    $addids = array();
    //$customer_address = \app\models\UserAddress::find()->where(['user_id'=>$model->id, 'status'=>'1'])->orderBy(['address_id'=>SORT_DESC])->asArray()->all();
    $customer_address = \app\models\AddressDetail::find()->where(['customer_id'=>$model->id])->orderBy(['id'=>SORT_DESC])->asArray()->all();
    for($a=0; $a<count($customer_address); $a++)
    {
        //$addids[] = $customer_address[$a]['address_id'];
        $addids[] = $customer_address[$a]['id'];
    }
    $address = \app\models\Address::find()->where(['id'=>$addids])->orderBy(['id'=>SORT_DESC])->asArray()->all();

    for($a=0; $a<count($address); $a++)
    {
        $address[$a]['is_default'] = !empty($address[$a]['is_default'])?$address[$a]['is_default']:'0'; 
        $add_detail = \app\models\AddressDetail::find()->where(['id'=>$address[$a]['id']])->one();
        if(!empty($add_detail))
        {
            foreach($add_detail as $key => $val)
            {
                $address[$a][$key] = $val; 
            }
        }
    }
     * 
     */
    
    $customer_address = \app\models\AddressDetail::find()->where(['customer_id'=>$model->id])->orderBy(['id'=>SORT_DESC])->asArray()->all();
    for($a=0; $a<count($customer_address); $a++)
    {
        $add_detail = $customer_address[$a];
        if(!empty($add_detail))
        {
            foreach($add_detail as $key => $val)
            {
                $address[$a][$key] = $val; 
            }
        }
    }
    //print_r($address[0]);exit;
    $address_type = ["","Apartement", "House", "Office", "Store"];
    for($a=0; $a<count($customer_address); $a++){
        if(isset($address[$a]['id'])){
        if(empty($address[$a]['address_type']))
            $address[$a]['address_type'] = 0;
        //print_r($address[$a]);
        echo "Address Name : ".$address[$a]['area_en']."<br/>";
        //echo "Address Type : ".$address_type[$address[$a]['address_type']]."<br/>";
        //echo "Block : ".$address[$a]['block']."<br/>";
        echo "Street : ".$address[$a]['street']."<br />";
        echo "Building : ".$address[$a]['building']."<br/>";
        echo "Floor : ".$address[$a]['floor']."<br/>";
        echo "Apartment Number : ".$address[$a]['apartement_number']."<br/>";
        echo "<br/>";
        }
    }
    ?>
    

</div>
