<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MenuAvailability */

$this->title = 'Create Menu Availability';
$this->params['breadcrumbs'][] = ['label' => 'Menu Availabilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-availability-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
