<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MenuAvailabilitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menu Availabilities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-availability-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Menu Availability', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'date_from',
            'date_to',
            [
            'attribute' => 'package_id',
            'format' => 'raw',
            'value' => function ($model) {
                $pack = \app\models\Package::find()
                        ->where("id=:pid",['pid'=>$model->package_id])
                        ->one();
                return isset($pack->title_en)?$pack->title_en:"";
            }],
            [
            'attribute' => 'menu_id',
            'format' => 'raw',
            'value' => function ($model) {
                $pack = \app\models\Menu::find()
                        ->where("id=:mid",['mid'=>$model->menu_id])
                        ->one();
                return isset($pack->title_en)?$pack->title_en:"";
            }],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
