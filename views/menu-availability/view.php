<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MenuAvailability */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Menu Availabilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="menu-availability-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date_from',
            'date_to',
            [
            'attribute' => 'package_id',
            'format' => 'raw',
            'value' => function ($model) {
                $pack = \app\models\Package::find()
                        ->where("id=:pid",['pid'=>$model->package_id])
                        ->one();
                return isset($pack->title_en)?$pack->title_en:"";
            }],
            [
            'attribute' => 'menu_id',
            'format' => 'raw',
            'value' => function ($model) {
                $pack = \app\models\Menu::find()
                        ->where("id=:mid",['mid'=>$model->menu_id])
                        ->one();
                return isset($pack->title_en)?$pack->title_en:"";
            }],
        ],
    ]) ?>

</div>
