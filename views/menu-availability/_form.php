<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\MenuAvailability */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-availability-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date_from')->widget(DatePicker::className(),[
	//'name' => 'check_issue_date', 
	'value' => date('d-M-Y', strtotime('+2 days')),
	'options' => ['placeholder' => 'Select coupon date start'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'todayHighlight' => true
	]
    ]) ?>


    <?= $form->field($model, 'date_to')->widget(DatePicker::className(),[
	//'name' => 'check_issue_date', 
	'value' => date('d-M-Y', strtotime('+2 days')),
	'options' => ['placeholder' => 'Select coupon date end'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'todayHighlight' => true
	]
    ]) ?>

    <?php
    
    $package = ArrayHelper::map(\app\models\Package::find()->all(),'id','title_en');
    echo $form->field($model, 'package_id')->widget(Select2::classname(), [
            'data' => $package,
            'options' => ['placeholder' => 'Select a package ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); 
    if($model->isNewRecord){
        $ref_arr = [];
    }else{
        $ref_arr = ArrayHelper::map(\app\models\Menu::find()->where(['status'=>1, 'package_id'=>$model->package_id])->all(),'id','title_en');
        //print_r($ref_arr);
    }
    echo "<div id='menu_box'>";
        echo $form->field($model, 'menu_id')->widget(DepDrop::classname(), [
            'type'=>DepDrop::TYPE_SELECT2,
            'data'=>$ref_arr,
            'options' => ['id' => 'menu_id'],
            'pluginOptions' => [
                'depends' => ['menuavailability-package_id'],
                'placeholder' => 'Select...',
                'url' => Url::to(['/package/menu_list   '])
            ],
            
        ]);
        echo "</div>";
    ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
