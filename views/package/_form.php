<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;


use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Package */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="package-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_ar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_en')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description_ar')->textarea(['rows' => 6]) ?>

    <?=(!empty($model->image))?"<img src='".Url::base(true).'/upload_file/package/'.$model->image."' class='img img-responsive' style='max-width:200px'>":''?>
    <?= $form->field($model, 'image_src')->widget(FileInput::classname(), [
                          'options' => ['accept' => 'image/*'],
                           'pluginOptions'=>['allowedFileExtensions'=>['jpeg','jpg','gif','png'],'showUpload' => false,'showPreview' => false,],
                      ]);   ?>
    
    <label>Category</label>
    <?php
    $category = \app\models\Category::find()->all();
    $category_arr = ArrayHelper::map($category, 'id', 'title_en');
    $category_ids = 0;
    
    echo Select2::widget([
    // 'model' => $model,
    'name' => 'category', 'value' => $category_ids, 'id' => 'category', 
        'data' => $category_arr, 'showToggleAll' => false, 
        'toggleAllSettings' => [
            'selectLabel' => '<i class="fas fa-ok-circle"></i> Tag All',
            'unselectLabel' => '<i class="fas fa-remove-circle"></i> Untag All',
            'selectOptions' => ['class' => 'text-success'],
            'unselectOptions' => ['class' => 'text-danger'],
        ],
        'options' => ['placeholder' => 'Select Category', 'class' => 'form-control', 'multiple' => true, ], 'pluginOptions' => [
    //'showToggleAll' => false,
    //'maximumSelectionLength' => 2, 
    ], ]);
    ?>
    <?= $form->field($model, 'status')->dropDownList([ '0'=>'Inactive', '1'=>'Active', ], ['prompt' => '-Select Status-']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
