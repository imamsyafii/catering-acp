<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Package */

$this->title = $model->title_en;
$this->params['breadcrumbs'][] = ['label' => 'Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="package-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title_en',
            'title_ar',
            'description_en:ntext',
            'description_ar:ntext',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function ($model) { 
	               $return = !empty($model->image)? "<img src='".Url::base(true).'/upload_file/package/'.$model->image."' class='img img-responsive' style='max-width:200px'>":'';
                   return $return;
                },
              ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->status=='1')?'Active':'Inactive';
                }
            ]
        ],
    ]) ?>
    <?= Html::a('<i class="fa fa-plus"></i> Add New Menu', ['menu/create', 'pid'=>$model->id], ['class' => 'btn btn-info']) ?>
    <div class="row" style="margin-top: 10px;">
        
        <?php
        $menus = \app\models\Menu::find()
                ->where("package_id=:pid",['pid'=>$model->id])
                ->asArray()
                ->all();
        $attributes = \app\models\MenuAttribute::find()
                ->asArray()
                ->all();
        ?>
        <?php 
        foreach($menus as $mkey=>$mval)
        {
            ?>
        <div class="col-lg-6">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= strlen($mval['title_en'])<40?$mval['title_en']:substr($mval['title_en'],0,40).'...'?></h3>
                  <div class="box-tools pull-right">
                      <a href="<?=Url::base(true).'/menu/update/'.$mval['id'].'?pid='.$model->id?>" class="btn btn-box-tool" ><i class="fa fa-pencil"></i>
                    </a>
<!--                     <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-trash"></i></button> -->
                    <a href="<?=Url::base(true).'/menu/remove/'.$mval['id'].'?pid='.$model->id?>" class="btn btn-box-tool" ><i class="fa fa-trash"></i>
                    </a>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <?=$mval['title_en']?><br/>
                  <p><?=$mval['description_en']?> </p>
                  <p><?=(!empty($mval['image']))?"<img src='".Url::base(true).'/upload_file/menu/'.$mval['image']."' class='img img-responsive' style='max-width:200px'>":''?></p>
                  <?php
                  if(!empty($attributes))
                    {
                    echo "<h3>Attributes</h3>"
                        . "<table class='table table-bordered'>";
                    foreach($attributes as $att_key=>$att_val){
                        $value = \app\models\MenuAttributeValue::find()
                                ->where("menu_id=:mid and attribute_id=:aid", ['mid'=>$mval['id'], 'aid'=>$att_val['id']])
                                ->asArray()
                                ->one();
                        echo "<tr><td>".$att_val['title_en']."</td><td>".(!empty($value['value'])?$value['value']:'0')."</td></tr>";
                    }
                    echo "</table>";

                    }?>
                </div>
                <!-- /.box-body -->
              </div>
        </div>
        
        <?php
        }
        ?>
        
    </div>
</div>
