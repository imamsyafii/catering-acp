<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Packages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Create Package', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'title_en',
            'title_ar',
            'description_en:ntext',
            'description_ar:ntext',
            //'image',
            //'status',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}<br/>{add_menu}',
                'buttons' => [
                    'add_menu' => function ($url, $model) {
                        return Html::a('<i class="fa fa-plus"></i> Add New Menu', ['menu/create', 'pid'=>$model->id], ['class' => 'btn btn-info btn-xs']);
                    },
                ],
                
                ],
        ],
    ]); ?>


</div>
