<?php

use yii\helpers\Html;
use yii\grid\GridView;


use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;


$session = Yii::$app->session;
        
$user_access = $session->get('user_access');
$user_roles = $session->get('user_roles');
$user_detail = $session->get('user_detail');
$role = ($user_roles['item_name']);
$fullname = ($user_detail['fullname']);

$username = $user_access['username'];
$restaurant_id = NULL;
$branch_id = NULL;

?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            //'auth_key',
            //'password_hash',
            //'confirmation_token',
            // 'status',
            // 'superadmin',
            // 'created_at',
            // 'updated_at',
            // 'lastpass_changed',
            // 'last_wrong_login',
            // 'wrong_count',
            // 'registration_ip',
            // 'bind_to_ip',
            [
                    'attribute'=>'gridRoleSearch',
                    'filter'=>($role=="RCPSuperAdmin" || $role=="RCPAdmin")?array('RCPDriver'=>'Driver'):ArrayHelper::map(\app\models\AuthItem::find()->where("type=:type",['type'=>'1'])->all(),'name', 'description'),
                    'value'=>function(\app\models\User $model){
                                    return implode(', ', ArrayHelper::map($model->itemNames, 'name', 'description'));
                            },
                    'format'=>'raw',
                    //'visible'=>User::hasPermission('viewUserRoles'),
            ],
             'email:email',
            // 'email_confirmed:email',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
