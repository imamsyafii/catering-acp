<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

if(!($model->isNewRecord))
{
    $model->fullname  = isset($detail->fullname)?$detail->fullname:'';
    $model->phone_number  = isset($detail->phone_number)?$detail->phone_number:'';
    $model->role  = isset($role->item_name)?$role->item_name:'';
    $shop_id = $detail['shop_id'];
    $model->shop_id= $shop_id;
}
//print_r($user_roles->item_name);


$session = Yii::$app->session;
        
$user_access = $session->get('user_access');
$user_roles = $session->get('user_roles');
$user_detail = $session->get('user_detail');
$role = $user_roles['item_name'];
$fullname = ($user_detail['fullname']);

$username = $user_access['username'];
$shop_id = NULL;



if($role=="VendorAdmin")
{
    $shop_id = $user_detail['shop_id'];
    $extraFilter = " and shop_id='$shop_id' ";
    
    $model->shop_id= $shop_id;
}

else  if($role=="Admin")
{
    $extraFilter = "";
}
else
{
    $extraFilter = " and shop_id='abcd' ";
} 




?>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

    <script type="text/javascript">
        
        $(document).ready(function(){
        	if ( $(".field-user-phone_number>.help-block").length ) { 
  	$(".field-user-phone_number>.help-block").html('<i>Phone number should consist of 7 digits or more</i>');
  }
  
            if($( "#user-role" ).val()=='VendorAdmin')
                {
                    $('#box-shop').show();
                   
                }
                else
                {
                    $('#box-restaurant').hide();
                    $('#box-branch').hide();
                }
            $( "#user-role" ).change(function() {
                
               if($( "#user-role" ).val()=='VendorAdmin')
                {
                    $('#box-shop').show();
                   
                }
                else
                {
                    $('#box-shop').hide();
                    
                }
            });
           //$('input').disableAutoFill(); 
    });
    </script>




<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'fullname')->textInput(['maxlength' => true, 'required'=>'required']) ?>
    
    <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'required'=>'required']) ?>
    
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255]) ?>
	<div style="position:absolute;height:0px; overflow:hidden; ">
   <input type="text" name="fake_safari_username" >
   <input type="password" name="fake_safari_password">
</div>
    <?= $form->field($model, 'repeat_password')->passwordInput(['maxlength' => 255]) ?>
    
    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true, 'required'=>'required']) ?>
    
    <?= $form->field($model, 'email')->textInput(['maxlength' => 255, 'required'=>'required', 'type'=>'email']) ?>
    <?= $form->field($model, 'email_confirmed')->checkbox() ?>
    <?php
    
    if($role=="VendorAdmin")
    {
    	$model->role = 'VendorAdmin';
	echo $form->field($model, 'role')->hiddenInput(['maxlength' => true])->label(false) ;
    }
    else
    {
    $role_list = array('Admin'=>'Top Management');
    echo  $form->field($model, 'role')->dropDownList($role_list, ['prompt' => '-Select Status-']) ;
    }
    ?>
    
    
    
    <?php 
    //print_r(\app\vendor\pixil\pxlocation\models\Country::find()->all());exit;
    //$varss = array('CountryId', 'EnglishName');
    $shop = [];// ArrayHelper::map(\app\models\BusinessShop::find()->where(" Status='1'  ",[])->all(),'id','title_en');
    //print_r($country);
    ?> <!--
    <?php
    if(empty($shop_id))
                {
    ?>
    <div id="box-shop">
    <?php
    echo $form->field($model, 'shop_id')->widget(Select2::classname(), [
    'model' => $model,
    'attribute' => 'shop_id',
    'name' => 'shop_id',
    'data' => $shop,
    'options' => [
        'placeholder' => 'Select Shop',
        'class' => 'form-control',
        'multiple' => false
    ], ]);
    ?>
    <?php
    }
                else
                {
                	$model->shop_id = $shop_id;
	                echo $form->field($model, 'shop_id')->hiddenInput(['maxlength' => true])->label(false) ;
                }
    ?>
    </div>
    -->
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
