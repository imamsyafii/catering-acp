<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = isset($detail->fullname)?$detail->fullname:$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1><?=isset($detail->fullname)?$detail->fullname:Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'auth_key',
            'password_hash',
            'confirmation_token',
            //'status',
            //'superadmin',
            'created_at',
            'updated_at',
            //'lastpass_changed',
            //'last_wrong_login',
            //'wrong_count',
            'registration_ip',
            //'bind_to_ip',
            'email:email',
            //'email_confirmed',
        ],
    ]) ?>
    
    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
            <tr><th width="20%">Fullname</th><td><?=isset($detail->fullname)?$detail->fullname:''?></td></tr>
            <tr><th>Phone Number</th><td><?=isset($detail->phone_number)?$detail->phone_number:''?></td></tr>
            <tr><th>Role</th><td><?=isset($role->item_name)?$role->item_name:''?></td></tr>
        </tbody></table>

</div>
