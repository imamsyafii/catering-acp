<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<div class="row">
		<div class="col-md-4">
				<h3>Today's Order</h3>
				<table class="table table-striped table-bordered">
					<thead>
					<tr><td width="20px">No.</td><td>Menu</td></tr>
					</thead>
				</table>
		</div>
		<div class="col-md-8">
			<h3>Order List</h3>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'user_id',
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $cust = \app\models\Customer::find()
                            ->where('id=:uid',['uid'=>$model->user_id])
                            ->one();
                    $cust_name =  isset($cust->first_name)?"<h2 style='margin:0px;'>".$cust->first_name." ".$cust->last_name."</h2>":'';
                    if(!empty($cust_name)){
                        $add = \app\models\AddressDetail::find()
                                ->where("address_detail.id=:aid", ['aid'=>$model->address_id])
                                ->asArray()
                                ->one();
                        $address = "<hr style='margin:0; border-color:#ababab'/><b>Adress: </b><br/>";
                        if(!empty($add)){
                            $address.= "Block: ".$add['block']."<br/>";
                            $address.= "Street: ".$add['street']."<br/>";
                            $address.= "Avenue: ".$add['avenue']."<br/>";
                            $address.= "Building: ".$add['building']."<br/>";
                            $address.= "Floor: ".$add['floor']."<br/>";
                        }
                        $cust_name .= "".$address;
                       
                    }
                     return $cust_name;
                },
                //'filter'=>['0'=>'Inactive','1'=>'Active'],
            ],
            'order_reference',
            //'category_id',
            [
                'attribute' => 'category_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $cat = \app\models\Category::find()
                            ->where("id=:cid",['cid'=>$model->category_id])
                            ->one();
                    return isset($cat->title_en)?$cat->title_en:"";
                }],
            [
                'attribute' => 'package_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $pack = \app\models\Package::find()
                            ->where("id=:pid",['pid'=>$model->package_id])
                            ->one();
                    return isset($pack->title_en)?$pack->title_en:"";
                }],
            'order_from',
            'order_to',
            'order_status',
            'created_at',
            //'updated_at',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
	</div>


</div>
