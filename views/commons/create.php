<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Commons */

$this->title = 'Create Commons';
$this->params['breadcrumbs'][] = ['label' => 'Commons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commons-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
