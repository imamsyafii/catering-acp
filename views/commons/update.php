<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Commons */

$this->title = 'Update Commons: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Commons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="commons-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
