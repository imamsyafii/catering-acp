<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CommonsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Commons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commons-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Commons', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'common_key',
            //'common_value:ntext',
            [
                'attribute' => 'common_value',
                'format' => 'raw',
                'value' => function ($model) {
                    return strip_tags($model->common_value);
                }
            ],
            'description:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
